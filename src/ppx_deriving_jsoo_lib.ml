open Ppxlib
open Ast_builder.Default
module Common = Common
module Jsoo_type = Jsoo_type
module Jsoo_conv = Jsoo_conv
module Ppx_js = Ppx_js
open Common

module Base = struct
  let jsoo_types ~options l =
    let l = List.map (fun t ->
      let loc = t.ptype_loc in
      let name = t.ptype_name.txt in
      let name_js = mkl ~loc @@ jsoo_name ~options name in
      let params = t.ptype_params in
      let ct_or_t, ct_ext = Jsoo_type.declaration_of_type_kind ~options t in
      match ct_or_t with
      | CT expr ->
        ct_ext @ [class_infos ~loc ~virt:Concrete ~params ~name:name_js ~expr], None, true
      | TT c -> ct_ext, Some (
        type_declaration ~loc ~name:name_js ~params ~cstrs:[]
          ~kind:Ptype_abstract ~manifest:(Some c) ~private_:Public), false
    ) l in
    List.flatten @@ List.map (fun (x, _, _) -> x) l,
    List.filter_map (fun (_, x, _) -> x) l,
    List.map (fun (_, _, x) -> x) l

  let jsoo_convs ~options l =
    List.map (fun t ->
      let cv = Jsoo_conv.conv_expressions ~options t in
      t, cv) l

  let jsoo_str ~loc ~rec_flag ~recursive ~options l =
    let cts, ts, are_class_type = jsoo_types ~options l in
    let convs = jsoo_convs ~options l in
    let vals, conv = List.split @@ List.map2 (fun (t, cv) is_class_type ->
      let loc = t.ptype_loc in
      let name = t.ptype_name.txt in
      let _name_js, name_to, name_of, name_conv =
        jsoo_name ~options name, jsoo_name_to ~options name, jsoo_name_of ~options name, jsoo_name_conv ~options name in
      let to_sig, of_sig, conv_sig = Jsoo_conv.conv_signatures ~options ~is_class_type t in
      let acc = List.flatten @@ List.map (fun ({e_to; e_of; _}, (name_to, name_of)) ->
        [ value_binding ~loc ~pat:(pvar ~loc name_to) ~expr:e_to;
          value_binding ~loc ~pat:(pvar ~loc name_of) ~expr:e_of ]) cv.e_acc in
      acc @ [ value_binding ~loc ~pat:(ppat_constraint ~loc (pvar ~loc name_to) to_sig)
                ~expr:cv.e_to;
              value_binding ~loc ~pat:(ppat_constraint ~loc (pvar ~loc name_of) of_sig)
                ~expr:cv.e_of ],
      value_binding ~loc ~pat:(ppat_constraint ~loc (pvar ~loc name_conv) conv_sig)
        ~expr:(pexp_tuple ~loc [evar ~loc name_to; evar ~loc name_of])
    ) convs are_class_type in
    let rec_flag_type = match recursive with
      | None -> rec_flag | Some true -> Recursive | Some false -> Nonrecursive in
    let vals = List.flatten vals in
    let rec_flag_conv = match recursive, List.length vals with
      | Some true, _ -> Recursive
      | Some false, _ -> Nonrecursive
      | None, i when i <= 2 -> Nonrecursive
      | _ -> Recursive in
    pstr_class_type ~loc cts :: (match ts with [] -> [] | _ -> [ pstr_type ~loc rec_flag_type ts ])
    @ [ pstr_value ~loc rec_flag_conv vals; pstr_value ~loc Nonrecursive conv ]

  let jsoo_sig ~loc ~rec_flag ~options l =
    let cts, ts, are_class_type = jsoo_types ~options l in
    let sigs = List.flatten @@ List.map2 (fun t is_class_type ->
      let loc = t.ptype_loc in
      let name = t.ptype_name.txt in
      let _name_js, name_to, name_of, name_conv =
        jsoo_name ~options name, jsoo_name_to ~options name, jsoo_name_of ~options name, jsoo_name_conv ~options name in
      let to_sig, of_sig, conv_sig = Jsoo_conv.conv_signatures ~options ~is_class_type t in
      [ value_description ~loc ~name:{txt=name_to; loc} ~type_:to_sig ~prim:[];
        value_description ~loc ~name:{txt=name_of; loc} ~type_:of_sig ~prim:[];
        value_description ~loc ~name:{txt=name_conv; loc} ~type_:conv_sig ~prim:[];
      ]) l are_class_type in
    let s = psig_class_type ~loc cts :: (
      match ts with [] -> [] | _ -> [ psig_type ~loc rec_flag ts ]) in
    s @ List.map (psig_value ~loc) sigs

  let jsoo_types_str ~loc ~rec_flag ~recursive ~options l =
    let rec_flag = match recursive with
      | None -> rec_flag | Some true -> Recursive | Some false -> Nonrecursive in
    let cts, ts, _ = jsoo_types ~options l in
    pstr_class_type ~loc cts :: (
      match ts with [] -> [] | _ -> [ pstr_type ~loc rec_flag ts ])

  let jsoo_convs_str ~loc ~recursive ~options l =
    let l = jsoo_convs ~options l in
    let vals, conv = List.split @@ List.map (fun (t, cv) ->
      let loc = t.ptype_loc in
      let name = t.ptype_name.txt in
      let name_to, name_of, name_conv =
        jsoo_name_to ~options name, jsoo_name_of ~options name, jsoo_name_conv ~options name in
      let acc = List.flatten @@ List.map (fun ({e_to; e_of; _}, (name_to, name_of)) ->
        [ value_binding ~loc ~pat:(pvar ~loc name_to) ~expr:e_to;
          value_binding ~loc ~pat:(pvar ~loc name_of) ~expr:e_of ]) cv.e_acc in
      acc @ [ value_binding ~loc ~pat:(pvar ~loc name_to) ~expr:cv.e_to;
              value_binding ~loc ~pat:(pvar ~loc name_of) ~expr:cv.e_of ],
      value_binding ~loc ~pat:(pvar ~loc name_conv)
        ~expr:(pexp_tuple ~loc [evar ~loc name_to; evar ~loc name_of])) l in
    let vals = List.flatten vals in
    let rec_flag = match recursive, List.length vals with
      | Some true, _ -> Recursive
      | Some false, _ -> Nonrecursive
      | None, i when i <= 2 -> Nonrecursive
      | _ -> Recursive in
    [ pstr_value ~loc rec_flag vals; pstr_value ~loc Nonrecursive conv ]
end

let rm_prefix_of_expr e : rm_prefix option = match e.pexp_desc with
  | Pexp_construct ({txt=Lident "true"; _}, _) -> Some (`bool true)
  | Pexp_construct ({txt=Lident "false"; _}, _) -> Some (`bool false)
  | Pexp_constant (Pconst_integer (s, None)) -> Some (`length (int_of_string s))
  | Pexp_constant (Pconst_string (s, _, _)) -> Some (`prefix s)
  | _ -> None

let parse_env l = List.iter (fun (n, e) ->
  let n = Longident.name n.txt in
  match n, e.pexp_desc with
  | "debug", Pexp_construct ({txt=Lident "false"; _}, None) -> Common.verbose := 0
  | "debug", _ -> Common.verbose := 1
  | "fake", Pexp_construct ({txt=Lident "false"; _}, None) -> Common.fake := false
  | "fake", _ -> Common.fake := true
  | "bigint", Pexp_construct ({txt=Lident "false"; _}, None) -> Common.bigint := false
  | "bigint", _ -> Common.bigint := true
  | _ -> ()) l

module Ppx_deriver = struct
  open Base

  let options case rm_prefix mut g_modules enum camel snake f_rm_undefined case_kind : rm_prefix options =
    let f_case = if camel then `camel else if snake then `snake else `default in
    let c_variant = match enum, case, case_kind with
      | Some x, _, _ -> x
      | _, Some c, false -> `case c
      | _, Some c, true ->  `case_kind c
      | _ -> `default in
    let f_prop = if mut then `mutable_ else `readonly in
    let g_rm_prefix = Option.value ~default:(`bool true) rm_prefix in
    let g_modules = Option.value ~default:[] g_modules in
    let options = { default_options with f_case; c_variant; g_modules; f_prop; g_rm_prefix; f_rm_undefined } in
    options

  let str_jsoo_types ~loc ~path:_ (rec_flag, types) case force rm_prefix
      recursive mut modules enum camel snake rm_undefined case_kind =
    if !fake then []
    else
      let options =
        options case rm_prefix mut modules enum camel snake rm_undefined case_kind in
      let str = jsoo_types_str ~loc ~rec_flag ~recursive ~options types in
      debug ~force "%s" (str_of_structure str);
      str

  let sig_jsoo_types ~loc ~path:_ (rec_flag, l) case rm_prefix recursive mut
      modules enum camel snake case_kind =
    if !fake then []
    else
      let rec_flag = match recursive with
        | None -> rec_flag | Some true -> Recursive | Some false -> Nonrecursive in
      let options = options case rm_prefix mut modules enum camel snake false case_kind in
      let cts, ts, _ = jsoo_types ~options l in
      let s = psig_class_type ~loc cts :: (
        match ts with [] -> [] | _ -> [ psig_type ~loc rec_flag ts ]) in
      s

  let str_jsoo_convs ~loc ~path:_ (_rec_flag, l) case force rm_prefix
      recursive mut modules enum camel snake rm_undefined case_kind =
    if !fake then []
    else
      let options = options case rm_prefix mut modules enum camel snake rm_undefined case_kind in
      let str = jsoo_convs_str ~loc ~recursive ~options l in
      debug ~force "%s" (str_of_structure str);
      Ppx_js.transform#structure str

  let str_gen ~loc ~path:_ (rec_flag, l) case force rm_prefix recursive mut
      modules enum camel snake rm_undefined case_kind =
    if !fake then []
    else
      let options = options case rm_prefix mut modules enum camel snake rm_undefined case_kind in
      let str = jsoo_str ~loc ~rec_flag ~recursive ~options l in
      debug ~force "%s" (str_of_structure str);
      Ppx_js.transform#structure str

  let sig_gen ~loc ~path:_ (rec_flag, l) case rm_prefix recursive mut modules enum camel snake case_kind =
    if !fake then []
    else
      let rec_flag = match recursive with None -> rec_flag | Some true -> Recursive | Some false -> Nonrecursive in
      let options = options case rm_prefix mut modules enum camel snake false case_kind in
      let s = jsoo_sig ~loc ~rec_flag ~options l in
      Ppx_js.transform#signature s

  let ebool t =
    let f = Deriving.Args.to_func t in
    Deriving.Args.of_func (fun ctx loc x k ->
      match x.pexp_desc with
      | Pexp_construct ({txt=Lident "true"; _}, _) -> f ctx loc true k
      | Pexp_construct ({txt=Lident "false"; _}, _) -> f ctx loc false k
      | _ -> Location.raise_errorf ~loc "wrong boolean argument")

  let eprefix t =
    let f = Deriving.Args.to_func t in
    Deriving.Args.of_func (fun ctx loc x k ->
      match rm_prefix_of_expr x with
      | Some x -> f ctx loc x k
      | None -> Location.raise_errorf ~loc "wrong boolean argument")

  let construct_pair t =
    let f = Deriving.Args.to_func t in
    Deriving.Args.of_func (fun ctx loc x k ->
      match x.pexp_desc with
      | Pexp_tuple [ {pexp_desc=Pexp_construct ({txt=id1; _}, None); _};
                     {pexp_desc=Pexp_construct ({txt=id2; _}, None); _} ] ->
        f ctx loc (Longident.name id1, Longident.name id2) k
      | _ -> Location.raise_errorf ~loc "wrong ident pair argument")

  let enum t =
    let f = Deriving.Args.to_func t in
    Deriving.Args.of_func (fun ctx loc x k ->
      match x.pexp_desc with
      | Pexp_construct ({txt=Lident "String"; _}, _) -> f ctx loc `string_enum k
      | Pexp_construct ({txt=Lident "Int"; _}, _) -> f ctx loc `int_enum k
      | _ -> f ctx loc `string_enum k)

  let string_or_flag t =
    let f = Deriving.Args.to_func t in
    Deriving.Args.of_func (fun ctx loc x k ->
      match x.pexp_desc with
      | Pexp_constant Pconst_string (s, _, _) -> f ctx loc (Some s) k
      | _ -> f ctx loc None k)

  let rec include_jsoo_ext str =
    List.fold_left (fun acc it ->
      match it.pstr_desc with
      | Pstr_module ({ pmb_expr = { pmod_desc = Pmod_structure str; _ }; _ } as m) ->
        let str = include_jsoo_ext str in
        let m = { m with pmb_expr = { m.pmb_expr with pmod_desc = Pmod_structure str } } in
        acc @ [ { it with pstr_desc = Pstr_module m } ]
      | Pstr_attribute {attr_name={txt="jsoo"; _}; attr_payload=PStr s; _}
      | Pstr_extension ( ({txt="jsoo"; _}, PStr s) , _) ->
        begin match s with
          | [ {pstr_desc=Pstr_eval ({pexp_desc=Pexp_record (l, None); _}, _); _} ] ->
            parse_env l;
            acc
          | _ ->
            if !fake then acc else acc @ (Ppx_js.transform#structure s)
        end
      | _ -> acc @ [ it ]) [] str

  let args_str = Deriving.Args.(
    empty
    +> arg "case" (string_or_flag __)
    +> flag "debug"
    +> arg "remove_prefix" (eprefix __)
    +> arg "recursive" (ebool __)
    +> flag "mut"
    +> arg "modules" (elist (construct_pair __))
    +> arg "enum" (enum __)
    +> flag "camel"
    +> flag "snake"
    +> flag "remove_undefined"
    +> flag "case_kind"
  )

  let args_sig = Deriving.Args.(
    empty
    +> arg "case" (string_or_flag __)
    +> arg "remove_prefix" (eprefix __)
    +> arg "recursive" (ebool __)
    +> flag "mut"
    +> arg "modules" (elist (construct_pair __))
    +> arg "enum" (enum __)
    +> flag "camel"
    +> flag "snake"
    +> flag "case_kind"
  )

  let main () =
    Driver.register_transformation "jsoo" ~preprocess_impl:include_jsoo_ext;
    let str_type_decl = Deriving.Generator.make args_str str_gen in
    let sig_type_decl = Deriving.Generator.make args_sig sig_gen in
    Deriving.ignore @@ Deriving.add jsoo ~str_type_decl ~sig_type_decl;
    let str_type_decl = Deriving.Generator.make args_str str_jsoo_types in
    let sig_type_decl = Deriving.Generator.make args_sig sig_jsoo_types in
    Deriving.ignore @@ Deriving.add "jsoo_type" ~str_type_decl ~sig_type_decl;
    let str_type_decl = Deriving.Generator.make args_str str_jsoo_convs in
    Deriving.ignore @@ Deriving.add "jsoo_conv" ~str_type_decl;
end

module Deriver_exe = struct
  open Base

  type options = {
    case : string option option;
    remove_prefix : rm_prefix;
    recursive : bool option;
    mut : bool;
    debug : bool;
    modules : (string * string) list option;
    enum : [ `string_enum | `int_enum ] option;
    remove_undefined : bool;
    case_kind : bool;
    camel: bool;
    snake: bool;
  }

  let options = {
    case=None; remove_prefix=`bool true; recursive=None;
    mut=false; debug=false; modules=None; enum=None; remove_undefined=false;
    case_kind=false; camel=false; snake=false
  }

  let get_bool s e = match e.pexp_desc with
    | Pexp_construct ({txt=Lident "true"; _}, _) -> true
    | Pexp_construct ({txt=Lident "false"; _}, _) -> false
    | _ -> Location.raise_errorf ~loc:e.pexp_loc "wrong boolean argument for %S" s

  let get_prefix s e = match rm_prefix_of_expr e with
    | Some x -> x
    | None -> Location.raise_errorf ~loc:e.pexp_loc "wrong prefix argument for %S" s

  let rec get_list acc e = match e.pexp_desc with
    | Pexp_construct ({ txt = Lident "[]"; _ }, None) ->
      List.rev acc
    | Pexp_construct ({ txt = Lident "::"; _ }, Some arg) -> begin
        match arg.pexp_desc with
        | Pexp_tuple [hd; tl] -> get_list (hd :: acc) tl
        | _ ->  Location.raise_errorf ~loc:arg.pexp_loc "wrong list argument"
      end
    | _ -> Location.raise_errorf ~loc:e.pexp_loc "wrong list argument"

  let get_construct_pair s e =
    match e.pexp_desc with
    | Pexp_tuple [ {pexp_desc=Pexp_construct ({txt=id1; _}, None); _}; {pexp_desc=Pexp_construct ({txt=id2; _}, None); _} ] ->
      (Longident.name id1, Longident.name id2)
    | _ ->
      Location.raise_errorf ~loc:e.pexp_loc "wrong ident pair for %S" s

  let get_enum e = match e.pexp_desc with
    | Pexp_construct ({txt=Lident "String"; _}, _) -> Some `string_enum
    | Pexp_construct ({txt=Lident "Int"; _}, _) -> Some `int_enum
    | _ -> Some `string_enum

  let get_string_or_flag e = match e.pexp_desc with
    | Pexp_constant Pconst_string (s, _, _) -> Some (Some s)
    | _ -> Some None

  let get_options = function
    | {pexp_desc=Pexp_record (l, _); _} ->
      let l = List.filter_map (function ({txt=Lident s; _}, e) -> Some (s, e) | _ -> None) l in
      let opt = List.fold_left (fun acc (s, e) -> match s with
        | "case" -> {acc with case = get_string_or_flag e}
        | "remove_prefix" -> {acc with remove_prefix = get_prefix s e}
        | "recursive" -> {acc with recursive = Some (get_bool s e)}
        | "mut" -> {acc with mut = true}
        | "debug" -> {acc with debug = true}
        | "modules" -> {acc with modules = Some (List.map (get_construct_pair s) (get_list [] e))}
        | "enum" -> {acc with enum = get_enum e}
        | "remove_undefined" -> {acc with remove_undefined = true}
        | "case_kind" -> {acc with case_kind = true}
        | "camel" -> {acc with camel = true}
        | "snake" -> {acc with snake = true}
        | _ -> Location.raise_errorf ~loc:e.pexp_loc "argument %S not handled" s
      ) options l in
      Some opt
    | _ ->
      Some options

  let filter l =
    let t = List.hd @@ List.rev l in
    let rec iter = function
      | [] -> None
      | a :: tl ->
        if a.attr_name.txt <> "deriving" then iter tl
        else match a.attr_payload with
          | PStr str ->
            let rec aux = function
              | [] -> iter tl
              | it :: str ->
                match it.pstr_desc with
                | Pstr_eval ({pexp_desc=Pexp_ident {txt=Lident "jsoo"; _}; _}, _) ->
                  Some options
                | Pstr_eval ({pexp_desc=Pexp_apply ({pexp_desc=Pexp_ident {txt=Lident "jsoo"; _}; _}, [Nolabel, e]); _}, _) ->
                  get_options e
                | Pstr_eval ({pexp_desc=Pexp_tuple l; _}, _) ->
                  let rec aux2 = function
                    | [] -> aux str
                    | h :: etl -> match h with
                      | {pexp_desc=Pexp_ident {txt=Lident "jsoo"; _}; _} ->
                        Some options
                      | {pexp_desc=Pexp_apply ({pexp_desc=Pexp_ident {txt=Lident "jsoo"; _}; _}, [Nolabel, e]); _} ->
                        get_options e
                      | _ -> aux2 etl in
                  aux2 l
                | _ -> aux str in
            aux str
          | _ -> iter tl in
    iter t.ptype_attributes

  let process ~loc ?(kind=`all) ~rec_flag ~recursive ~options l =
    match kind with
    | `all -> jsoo_str ~loc ~rec_flag ~recursive ~options l
    | `typ -> jsoo_types_str ~loc ~rec_flag ~recursive ~options l
    | `conv -> jsoo_convs_str ~loc ~recursive ~options l

  let tr_options o =
    let f_case = if o.camel then `camel else if o.snake then `snake else `default in
    let c_variant = match o.enum, o.case, o.case_kind with
      | Some e, _, _ -> (e :> variant_kind)
      | _, Some c, false -> `case c
      | _, Some c, true ->  `case_kind c
      | _ -> `default in
    let f_prop = if o.mut then `mutable_ else `readonly in
    let g_rm_prefix = o.remove_prefix in
    let g_modules = Option.value ~default:[] o.modules in
    let f_rm_undefined = o.remove_undefined in
    { default_options with f_case; c_variant; g_modules; f_prop; g_rm_prefix; f_rm_undefined }

  let rec derive_str ?(loc=Location.none) ?kind name str =
    let str = List.fold_left (fun acc it ->
      match it.pstr_desc with
      | Pstr_type (rec_flag, l) ->
        begin match filter l with
          | None -> acc
          | Some o ->
            let options = tr_options o in
            let str = process ~loc:it.pstr_loc ?kind ~rec_flag
                ~recursive:o.recursive ~options l in
            Common.debug ~force:o.debug "%s" (Common.str_of_structure str);
            acc @ (Ppx_js.transform#structure str)
        end
      | Pstr_module p ->
        begin match p.pmb_name.txt with
          | None -> acc
          | Some name ->
            begin match p.pmb_expr.pmod_desc with
              | Pmod_structure str ->
                let str = derive_str ?kind name str in
                if List.length str <= 1 then acc
                else
                  let loc = p.pmb_expr.pmod_loc in
                  acc @ [
                    pstr_module ~loc @@
                    module_binding ~loc ~name:{txt=Some name; loc}
                      ~expr:(pmod_structure ~loc str) ]
              | _ -> acc
            end
        end
      | Pstr_attribute {attr_name={txt="jsoo"; _}; attr_payload=PStr s; _}
      | Pstr_extension ( ({txt="jsoo"; _}, PStr s) , _) ->
        begin match s with
          | [ {pstr_desc=Pstr_eval ({pexp_desc=Pexp_record (l, None); _}, _); _} ] ->
            parse_env l; acc
          | _ -> acc @ Ppx_js.transform#structure s
        end
      | _ -> acc) [] str in
    pstr_open ~loc (open_infos ~loc ~override:Fresh ~expr:(pmod_ident ~loc (Common.llid ~loc name))) :: str

  let main () =
    let filename = ref None in
    let outfile = ref None in
    let kind = ref `all in
    let specs = [
      "--output", Arg.String (fun s -> outfile := Some s), "output file";
      "-o", Arg.String (fun s -> outfile := Some s), "alias of --output";
      "--type-only", Arg.Unit (fun () -> kind := `typ), "only output type/class type";
      "--conv-only", Arg.Unit (fun () -> kind := `conv), "only output conversion function";
    ] in
    let usage = "derive_jsoo <options> <filename>" in
    Arg.parse specs (fun f -> filename := Some f) usage;
    match !filename with
    | None -> Arg.usage specs usage
    | Some f ->
      let name = String.capitalize_ascii @@ Filename.(remove_extension @@ basename f) in
      let ic = open_in f in
      let s = really_input_string ic (in_channel_length ic) in
      let lexbuf = Lexing.from_string s in
      let str = Parse.implementation lexbuf in
      let str = derive_str ~kind:!kind name str in
      match !outfile with
      | None -> Pprintast.structure Format.std_formatter str
      | Some f ->
        let oc = open_out f in
        Pprintast.structure (Format.formatter_of_out_channel oc) str

end
