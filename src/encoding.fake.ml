open Ppxlib
open Ast_builder.Default

let enc_name name =
  match List.rev @@ String.split_on_char '.' name with
  | [] -> assert false
  | "t" :: q -> String.concat "." @@ List.rev @@ "enc" :: q
  | _ -> name ^ "_enc"

let json_expr c = function
  | Some enc -> enc
  | None ->
    let rec aux c = match c.ptyp_desc with
      | Ptyp_constr (n, l) ->
        let e = evar ~loc:c.ptyp_loc (enc_name (Longident.name n.txt)) in
        begin match l with
          | [] -> e
          | _ -> eapply ~loc:c.ptyp_loc e @@ List.map aux l
        end
      | _ -> Location.raise_errorf "cannot infer encoding for jsoo conversion" in
    aux c
