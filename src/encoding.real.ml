let json_expr c = function
  | Some enc -> enc
  | None -> Ppx_deriving_encoding_lib.Encoding.core ~wrap:false c
