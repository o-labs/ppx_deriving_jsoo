open Ppxlib
open Ast_builder.Default
open Common

let jse ~loc s = evar ~loc (js_mod s)

let esome ~loc e =
  pexp_construct ~loc (mkl ~loc @@ Lident "Some") (Some (e ~loc))
let psome ~loc e =
  ppat_construct ~loc (mkl ~loc @@ Lident "Some") (Some (e ~loc))

let pexp_fun p e =
  pexp_fun ~loc:e.pexp_loc Nolabel None p e

let i_project ~loc n i =
  pexp_fun (
    ppat_tuple ~loc (List.init n (fun j -> if j = i then pvar ~loc "x" else ppat_any ~loc)))
    (evar ~loc "x")

let pexp_fun0 v e =
  pexp_fun (pvar ~loc:e.pexp_loc v) e

let id_fun ?typ loc = match typ with
  | None -> [%expr Fun.id]
  | Some t ->
    [%expr fun (x : [%t ptyp_constr ~loc (llid ~loc t) []]) -> x]

let rec pexp_fun_rec expr = function
  | [] -> expr
  | h :: t -> pexp_fun0 h (pexp_fun_rec expr t)

let pexp_funs ~loc n expr =
  let l = List.init n (fun _ -> new_var ()) in
  let rec aux = function
    | [] -> expr (List.map (evar ~loc) l)
    | h :: t -> pexp_fun0 h (aux t) in
  aux l

let rec eapplys_expr l var = match l with
  | [] -> var
  | h :: t ->
    let loc = var.pexp_loc in
    eapply ~loc h [eapplys_expr t var]

let rec eapplys l var = match l with
  | [] -> var
  | h :: t ->
    let loc = var.pexp_loc in
    eapply ~loc (evar ~loc h) [eapplys t var]

let pexp_fun1 ~loc f =
  let var = new_var () in
  pexp_fun0 var (f @@ evar ~loc var)

let prop_get ~loc obj prop =
  eapply ~loc (evar ~loc "(##.)") [ obj ; evar ~loc prop ]

let method_call ~loc obj meth args =
  eapply ~loc (evar ~loc "(##)") [ obj ; pexp_apply ~loc (evar ~loc meth) args ]

let prop_try ~loc obj prop f =
  eapply ~loc (evar ~loc "(##?)") [ obj ; eapply ~loc (evar ~loc prop) [ f ] ]

let prop_set ~loc obj prop value =
  eapply ~loc (evar ~loc "(:=)") [eapply ~loc (evar ~loc "(##.)") [ obj ; evar ~loc prop ]; value ]

let prop_kind = function
  | `writeonly -> `Writeonly
  | `mutable_ -> `Readwrite
  | `case -> `Case
  | `readonly -> `Readonly

let literal_object ~loc self_id fields =
  let mutable_flag = function
    | `Readonly | `Case -> Immutable
    | _ -> Mutable in
  let aux = function
    | Ppx_js.Val (name, kind, ov_flag, e) ->
      [ pcf_val ~loc (name, mutable_flag kind, Cfk_concrete (ov_flag, e)) ]
    | Ppx_js.Cases (names, es) ->
      List.map2 (fun n e ->
        pcf_val ~loc (n, Immutable, Cfk_concrete (Fresh, e))) names es
    | Ppx_js.Meth (name, pv_flag, ov_flag, e, _) ->
      [ pcf_method ~loc (name, pv_flag, Cfk_concrete (ov_flag, e)) ] in
  let self = ppat_tuple ~loc [ self_id; pbool ~loc true ] in
  pexp_extension ~loc (
    {txt="js"; loc},
    PStr [
      pstr_eval ~loc (
        pexp_object ~loc (
          class_structure ~self ~fields:(List.flatten @@ List.map aux fields)
        )
      ) []
    ]
  )

let param_name p =
  let loc = p.ptyp_loc in
  match p.ptyp_desc with
  | Ptyp_var x -> x
  | _ -> Location.raise_errorf ~loc "parameter is not a variable"

let rec add_params_fun expr = function
  | [] -> expr
  | ({ptyp_desc = Ptyp_var x; ptyp_loc=loc; _}, _) :: t ->
    pexp_fun (ppat_tuple ~loc [
      pvar ~loc ("_" ^ x ^ _to_jsoo); pvar ~loc ("_" ^ x ^ _of_jsoo)])
      (add_params_fun expr t)
  | _ :: t -> add_params_fun expr t

let mconv ?(acc=[]) eto eof = {e_of=eof; e_to=eto; e_acc = acc}

let conv_map f l =
  let _, l_to, l_of, l_acc = List.fold_left (fun (i, acc_to, acc_of, acc) x ->
    let cv = f i x in
    i+1, cv.e_to :: acc_to, cv.e_of :: acc_of, cv.e_acc :: acc) (0, [],[],[]) l in
  List.rev l_to, List.rev l_of, List.flatten (List.rev l_acc)

let ml_type_to_conv ~loc ~options s l = match s, l with
  | "unit", [] -> mconv [%expr fun _ -> ()] [%expr fun _ -> ()]
  | "int", [] | "Int.t", [] ->
    if options.c_number then
      mconv
        [%expr fun x -> [%e jse ~loc "number_of_float"] (float_of_int x)]
        [%expr fun x -> int_of_float ([%e jse ~loc "float_of_number"] x)]
    else
      mconv (id_fun ~typ:"int" loc) (id_fun ~typ:"int" loc)
  | "int32", [] | "Int32.t", [] ->
    if options.c_number then
      mconv
        [%expr fun x -> [%e jse ~loc "number_of_float"] (Int32.to_float x)]
        [%expr fun x -> Int32.of_float ([%e jse ~loc "float_of_number"] x)]
    else mconv [%expr Int32.to_int] [%expr Int32.of_int]
  | "int64", [] | "Int64.t", [] ->
    if !bigint then
      mconv
        (jse ~loc "BigInt.of_int64")
        [%expr fun x -> Int64.of_string ([%e jse ~loc "BigInt.to_string"] x)]
    else
      mconv
        [%expr fun x -> [%e jse ~loc "number_of_float"] (Int64.to_float x)]
        [%expr fun x -> Int64.of_float ([%e jse ~loc "float_of_number"] x)]
  | "nativeint", [] | "Nativeint.t", [] ->
    if !bigint then
      mconv
        (jse ~loc "BigInt.of_native")
        [%expr fun x -> Nativeint.of_string ([%e jse ~loc "BigInt.to_string"] x)]
    else
      mconv
        [%expr fun x -> [%e jse ~loc "number_of_float"] (Nativeint.to_float x)]
        [%expr fun x -> Nativeint.of_float ([%e jse ~loc "float_of_number"] x)]
  | "Z.t", [] ->
    if !bigint then
      mconv
        [%expr fun x -> [%e jse ~loc "BigInt.of_string"] (Z.to_string x)]
        [%expr fun x -> Z.of_string ([%e jse ~loc "BigInt.to_string"] x)]
    else
      mconv
        [%expr fun x -> [%e jse ~loc "string"] (Z.to_string x)]
        [%expr fun x -> Z.of_string ([%e jse ~loc "to_string"] x)]
  | "float", [] | "Float.t", [] ->
    mconv (jse ~loc "number_of_float") (jse ~loc "float_of_number")
  | "string", [] | "String.t", [] ->
    mconv (jse ~loc "string") (jse ~loc "to_string")
  | "bool", [] | "Bool.t", [] ->
    mconv (jse ~loc "bool") (jse ~loc "to_bool")
  | "char", [] | "Char.t", [] ->
    mconv
      [%expr fun x -> [%e jse ~loc "string"] (String.make 1 x)]
      [%expr fun x -> String.get ([%e jse ~loc "to_string"] x) 0]
  | "bytes", [] | "Bytes.t", [] ->
    let e_to = [%expr (fun b ->
      let a = Bigarray.Array1.create Bigarray.char Bigarray.c_layout (Bytes.length b) in
      Bytes.iteri (fun i c -> Bigarray.Array1.unsafe_set a i c) b;
      Typed_array.Bigstring.to_arrayBuffer a)] in
    mconv e_to
      [%expr fun x -> Bytes.of_string ([%e jse ~loc "Typed_array.String.of_arrayBuffer"] x)]
  | "array", [c]  | "Array.t", [c] ->
    mconv
      [%expr [%e jse ~loc "of_arrayf"] [%e c.e_to]]
      [%expr [%e jse ~loc "to_arrayf"] [%e c.e_of]]
  | "list", [c] | "List.t", [c] ->
    mconv
      [%expr [%e jse ~loc "of_listf"] [%e c.e_to]]
      [%expr [%e jse ~loc "to_listf"] [%e c.e_of]]
  | "option", [c] | "Option.t", [c] ->
    mconv
      [%expr [%e jse ~loc (match options.c_opt with `opt -> "opt" | `optdef -> "optdef" | `aopt -> "aopt")] [%e c.e_to]]
      [%expr [%e jse ~loc ("to_" ^ (match options.c_opt with `opt -> "opt" | `optdef -> "optdef" | `aopt -> "aopt"))] [%e c.e_of]]
  | "Unsafe.any", [] | "any", [] | "Js.Unsafe.any", [] | "Js_of_ocaml.Js.Unsafe.any", [] ->
    mconv (id_fun ~typ:(js_mod "Unsafe.any") loc) (id_fun ~typ:(js_mod "Unsafe.any") loc)
  | s, [] when s = js_mod "Unsafe.any"
    -> mconv (id_fun ~typ:(js_mod "Unsafe.any") loc) (id_fun ~typ:(js_mod "Unsafe.any") loc)
  | "ref", [c] ->
    mconv
      [%expr fun x -> [%e c.e_to] [%e pexp_field ~loc (evar ~loc "x") (llid ~loc "contents")]]
      [%expr fun x -> ref ([%e c.e_of] x)]
  | "Lazy.t", [c] ->
    mconv
      [%expr fun x -> [%e c.e_to] (Lazy.force x)]
      [%expr fun x -> Lazy.from_val ([%e c.e_of]  x)]
  | "Bigstring.t", [] | "Cstruct.buffer", [] ->
    mconv
      (jse ~loc "Typed_array.Bigstring.to_arrayBuffer")
      (jse ~loc "Typed_array.Bigstring.of_arrayBuffer")
  | "Hex.t", [] ->
    mconv
      [%expr fun x -> [%e jse ~loc "string"] (Hex.show x)]
      [%expr fun x -> `Hex ([%e jse ~loc "to_string"] x)]
  | _, l ->
    let to_jsoo, of_jsoo = jsoo_name_to ~options s, jsoo_name_of ~options s in
    let l = List.map (fun c -> pexp_tuple ~loc [c.e_to; c.e_of]) l in
    mconv
      (eapply ~loc (evar ~loc to_jsoo) l)
      (eapply ~loc (evar ~loc of_jsoo) l)

let arg_array ~loc l = pexp_array ~loc l

let add_expr0 ~loc ~options ~name (e_to, e_of) =
  let name_to, name_of = jsoo_name_to ~options name, jsoo_name_of ~options name in
  let v_to = value_binding ~loc ~pat:(pvar ~loc name_to) ~expr:e_to in
  let v_of = value_binding ~loc ~pat:(pvar ~loc name_of) ~expr:e_of in
  [v_to; v_of], (name_to, name_of)

let acc_expr ~loc ~options ~name cv =
  let name_to, name_of = jsoo_name_to ~options name, jsoo_name_of ~options name in
  let acc = cv.e_acc in
  let cv = mconv
      (add_params_fun cv.e_to options.g_params) (add_params_fun cv.e_of options.g_params) in
  let acc = acc @ [cv, (name_to, name_of)] in
  let e_to, e_of = match options.g_params with
    | [] -> evar ~loc name_to, evar ~loc name_of
    | _ ->
      let params = List.map (fun c ->
        let s = param_name c in
        pexp_tuple ~loc [evar ~loc ("_" ^ s ^ _to_jsoo); evar ~loc ("_" ^ s ^ _of_jsoo)])
        (Jsoo_type.core_of_param options.g_params) in
      eapply ~loc (evar ~loc name_to) params,
      eapply ~loc (evar ~loc name_of) params in
  mconv ~acc e_to e_of

let enum_variant ~options typ l =
  let typs = match typ with `string_enum -> "string" | `int_enum -> "int" in
  let ls, _, fallback = List.fold_left (fun (acc_l, i, fallback) rf ->
    let options = reset_options options in
    let loc = rf.prf_loc in
    match rf.prf_desc with
    | Rtag ({txt; loc}, _, []) ->
      let options = field_attributes ~options:{options with f_key=txt} rf.prf_attributes in
      let e = match options.f_code, typ with
        | Some c, `int_enum -> eint ~loc c, pint ~loc c
        | None, `int_enum -> eint ~loc i, pint ~loc i
        | _ -> estring ~loc options.f_key, pstring ~loc options.f_key in
      (txt, e) :: acc_l, (i+1), fallback
    | Rtag ({txt; _}, _, [{ptyp_desc = Ptyp_constr ({txt=Lident s; _}, []); _}]) when fallback = None && s = typs ->
      acc_l, (i+1), Some txt
    | _ -> Location.raise_errorf ~loc "variant case cannot be part of %s enum" typs) ([], 0, None) l in
  List.rev ls, fallback

let enum_constructor ~options typ l =
  let typs = match typ with `string_enum -> "string" | `int_enum -> "int" in
  let rm_prefix = prefix_length ~options @@ List.map (fun pcd -> pcd.pcd_name.txt) l in
  let ls, _, fallback = List.fold_left (fun (acc_l, i, fallback) pcd ->
    let options = reset_options options in
    let loc = pcd.pcd_loc in
    let name_cs = remove_prefix pcd.pcd_name.txt rm_prefix in
    let options = field_attributes ~options:{options with f_key=name_cs} pcd.pcd_attributes in
    let e = match options.f_code, typ with
      | Some c, `int_enum -> eint ~loc c, pint ~loc c
      | None, `int_enum -> eint ~loc i, pint ~loc i
      | _ -> estring ~loc options.f_key, pstring ~loc options.f_key in
    match pcd.pcd_args with
    | Pcstr_tuple [] -> (pcd.pcd_name.txt, e) :: acc_l, (i+1), fallback
    | Pcstr_tuple [ {ptyp_desc=Ptyp_constr ({txt=Lident s; _}, _); _} ]
      when fallback = None && typs = s ->
      acc_l, (i+1), Some pcd.pcd_name.txt
    | _ -> Location.raise_errorf ~loc:pcd.pcd_loc "variant case cannot be part of %s enum" typs) ([], 0, None) l in
  List.rev ls, fallback

let enum_expr ~loc ?fallback ?(kind=`Variant) typ l =
  let ppat_variant, pexp_variant = match kind with
    | `Variant -> ppat_variant ~loc , pexp_variant ~loc
    | `Construct ->
      (fun s p -> ppat_construct ~loc (llid ~loc s) p),
      (fun s e -> pexp_construct ~loc (llid ~loc s) e) in
  let apply_to e =
    match typ with
    | `string_enum -> eapply ~loc (jse ~loc "string") [e]
    | `int_enum -> eapply ~loc (jse ~loc "number_of_float") [ eapply ~loc (evar ~loc "float_of_int") [e] ] in
  let apply_of e =
    match typ with
    | `string_enum -> eapply ~loc (jse ~loc "to_string") [e]
    | `int_enum -> eapply ~loc (evar ~loc "int_of_float") [ eapply ~loc (jse ~loc "float_of_number") [e] ] in
  let e_to = pexp_function ~loc @@
    List.map (fun (ml, js) ->
      case ~guard:None ~lhs:(ppat_variant ml None)
        ~rhs:(apply_to (fst js))) l @ (
      match fallback with
      | None -> []
      | Some ml -> [
          case ~guard:None ~lhs:(ppat_variant ml (Some (pvar ~loc "x")))
            ~rhs:(apply_to (evar ~loc "x")) ]) in
  let e_of = pexp_fun1 ~loc (fun js ->
    pexp_match ~loc (apply_of js) @@
    List.map (fun (ml, js) ->
      case ~guard:None ~lhs:(snd js) ~rhs:(pexp_variant ml None)) l @ [
      match fallback with
      | None ->
        case ~guard:None ~lhs:(ppat_any ~loc)
          ~rhs:(eapply ~loc (evar ~loc "failwith") [ estring ~loc "no case matched" ])
      | Some ml ->
        case ~guard:None ~lhs:(pvar ~loc "x") ~rhs:(pexp_variant ml (Some (evar ~loc "x"))) ]) in
  mconv e_to e_of

type remember_item =
  ([ `RCase of string loc * (expression -> expression)
   | `RVal of string loc * [ `Case | `Readonly | `Readwrite | `Writeonly | `Optdef ] *
              override_flag * (expression -> expression)],
   expression -> expression -> expression) conv0

type remember = (add:value_binding list -> remember_item) list * string list

let remember_table : (string, remember) Hashtbl.t = Hashtbl.create 1024

let remember_fields id fields =
  Hashtbl.add remember_table id fields

let get_remember_fields id = Hashtbl.find_opt remember_table id

module FDMap = Map.Make(String)

let partition_rows l obj =
  List.map (function
    | `v (id, k, b, e) -> Ppx_js.Val (id, k, b, e obj)
    | `c (ids, es) -> Ppx_js.Cases (ids, List.map (fun e -> e obj) es)) @@
  snd @@ List.split @@ FDMap.bindings @@
  List.fold_left (fun acc r -> match r with
    | `RVal (id, k, b, e) ->
      FDMap.add (Ppx_js.unescape id.txt) (`v (id, k, b, e)) acc
    | `RCase (id, e) -> let txt = Ppx_js.unescape id.txt in
      match FDMap.find_opt txt acc with
      | Some (`c (ids, es)) ->
        FDMap.add txt (`c (ids @ [id], es @ [e])) acc
      | _ -> FDMap.add txt (`c ([id], [e])) acc) FDMap.empty l

let inherit_fields :
  (Longident.t, (expression -> Ppx_js.field_desc list) * string list * (expression -> (string * expression) list) option) Hashtbl.t =
  Hashtbl.create 512

let remove_undefined_expr ~loc expr =
  pexp_let ~loc Nonrecursive [ value_binding ~loc ~pat:(pvar ~loc "x") ~expr ] @@
  pexp_let ~loc Nonrecursive [
    value_binding ~loc ~pat:(punit ~loc)
      ~expr:(eapply ~loc (jse ~loc "remove_undefined") [ evar ~loc "x" ]) ] @@
  evar ~loc "x"

let kind_field_to ~loc ~kind ~name ?(case_option=false) fields_to l obj =
  let kind_field = pexp_match ~loc obj @@ List.map (fun (name, b) ->
    let lhs = match kind with
      | `Variant ->
        if b then ppat_variant ~loc name (Some (ppat_any ~loc))
        else ppat_variant ~loc name None
      | `Construct ->
        if b then ppat_construct ~loc (llid ~loc name) (Some (ppat_any ~loc))
        else ppat_construct ~loc (llid ~loc name) None in
    let lhs = if case_option then ppat_construct ~loc (llid ~loc "Some") (Some lhs) else lhs in
    let rhs = estring ~loc name in
    case ~guard:None ~lhs ~rhs
  ) l @ if case_option then [
    case ~guard:None ~lhs:(ppat_construct ~loc (llid ~loc "None") None) ~rhs:(estring ~loc "")
  ] else [] in
  (Ppx_js.Val ({txt="__kind_"^name^"_" ; loc}, `Readonly, Fresh, kind_field)) ::
  fields_to obj

let rec expr_of_core ~options c =
  let loc = c.ptyp_loc in
  let options = core_attributes ~options c.ptyp_attributes in
  match options.c_ignore, options.c_conv, options.c_json with
  | true, _, _ -> mconv (id_fun loc) (id_fun loc)
  | _, Some c, _ -> c
  | _, _, Some enc ->
    let enc = Encoding.json_expr c enc in
    mconv
      [%expr fun x -> Js_json.js_of_json @@ Json_encoding.construct [%e enc] x]
      [%expr fun x -> Json_encoding.destruct [%e enc] (Js_json.json_of_js x)]
  | _ ->
    match c.ptyp_desc with
    | Ptyp_any -> mconv (id_fun loc) (id_fun loc)
    | Ptyp_var v ->
      mconv (evar ~loc ("_" ^ v ^ "_" ^ to_jsoo)) (evar ~loc ("_" ^ v ^ "_" ^ of_jsoo))
    | Ptyp_constr ({txt; _}, l) ->
      let type_name = Longident.name txt in
      begin match Jsoo_type.assoc_type ~options type_name l with
        | None ->
          let l = List.map (fun c ->
            let options = reset_options options in
            expr_of_core ~options c) l in
          let cv = ml_type_to_conv ~loc ~options type_name l in
          let e_acc = List.fold_left (fun acc c -> acc @ c.e_acc) [] l in
          { cv with e_acc }
        | Some c ->
          let cv = expr_of_core ~options:(reset_options options) c in
          mconv ~acc:cv.e_acc
            [%expr [%e jse ~loc "Table.makef"] [%e cv.e_to]]
            [%expr [%e jse ~loc "Table.itemsf"] [%e cv.e_of]]
      end
    | Ptyp_arrow (_, c1, c2) -> function_expr ~loc ~options (c1, c2)
    | Ptyp_tuple l ->
      if options.c_array then array_tuple_expr ~loc ~options l
      else
        let cv1 = tuple_expr ~loc ~options l in
        let tuple_name = get_tuple_name options.g_name in
        acc_expr ~loc ~options ~name:tuple_name cv1
    | Ptyp_variant (l, _, _) ->
      begin match options.c_variant with
        | `string_enum | `int_enum as typ ->
          let ls, fallback = enum_variant ~options typ l in
          enum_expr ~loc ?fallback typ ls
        | _ ->
          if is_variant_enum (`variant l) then
            let ls, fallback = enum_variant ~options `string_enum l in
            enum_expr ~loc ?fallback `string_enum ls
          else
            let cv1 = variant_expr ~loc ~options l in
            let variant_name = get_variant_name options.g_name in
            acc_expr ~loc ~options ~name:variant_name cv1
      end
    | Ptyp_object (l, _) -> object_expr ~loc ~options l
    | _ -> Location.raise_errorf ~loc "core type not handled (only _, 'a, constr, arrow and tuple)"

and arrows_to_array ~options ?(meth_start=false) ?(callback_start=false) c =
  let loc = c.ptyp_loc in
  match c.ptyp_desc with
  | Ptyp_arrow (_a, c1, c2) ->
    let cv = expr_of_core ~options c1 in
    let es_to, es_of, vs_to, vs_of, e_to_end, e_of_end, acc = arrows_to_array ~options c2 in
    begin match meth_start, callback_start, c1 with
      | _, true, {ptyp_desc = Ptyp_constr ({txt; _}, _); _} when Longident.name txt = "unit" ->
        let v = Ppx_js.Arg.make1 ~ignore_:true () in
        es_to, eapply ~loc cv.e_of [evar ~loc (Ppx_js.Arg.name v)] :: es_of,
        v :: vs_to, v :: vs_of, e_to_end, e_of_end, acc @ cv.e_acc
      | true, _, {ptyp_desc = Ptyp_constr ({txt; _}, _); _} when Longident.name txt = "unit" ->
        let v = Ppx_js.Arg.make1 ~ignore_:true () in
        es_to, eunit ~loc :: es_of,
        v :: vs_to, vs_of, e_to_end, e_of_end, acc @ cv.e_acc
      | _ ->
        let v = Ppx_js.Arg.make1 () in
        eapply ~loc cv.e_to [evar ~loc (Ppx_js.Arg.name v)] :: es_to,
        eapply ~loc cv.e_of [evar ~loc (Ppx_js.Arg.name v)] :: es_of,
        v :: vs_to, v :: vs_of, e_to_end, e_of_end, acc @ cv.e_acc
    end
  | _ ->
    let cv = expr_of_core ~options c in
    [], [], [], [], cv.e_to, cv.e_of, cv.e_acc

and callback_expr ~loc ~options (c1, c2) =
  let options = reset_options options in
  let cv1 = expr_of_core ~options c1 in
  let cv2 = expr_of_core ~options c2 in
  let e_to = [%expr fun f -> [%e jse ~loc "wrap_callback"] (fun x ->
    [%e cv2.e_to] (f ([%e cv1.e_of] x)))] in
  let es_to, _, vs_to, _, _, e_of, acc =
    arrows_to_array ~options ~callback_start:true (ptyp_arrow ~loc Nolabel c1 c2) in
  let es_to = List.map (fun v -> eapply ~loc (jse ~loc "Unsafe.inject") [v]) es_to in
  let vs = List.map Ppx_js.Arg.name vs_to in
  let e_of = [%expr fun f -> [%e
    pexp_fun_rec (eapply ~loc e_of [
      eapply ~loc (jse ~loc "Unsafe.fun_call") [
        [%expr f]; arg_array ~loc es_to]])
      vs] ] in
  mconv ~acc:(acc @ cv1.e_acc @ cv2.e_acc) e_to e_of

and meth_callback_expr ~loc ~options (c1, c2) =
  let options = reset_options options in
  let cv1 = expr_of_core ~options c1 in
  let cv2 = expr_of_core ~options c2 in
  let f, x = new_var (), new_var () in
  let e_to = pexp_fun0 f (eapply ~loc (jse ~loc "wrap_meth_callback") [
    pexp_fun0 x (eapply ~loc cv2.e_to [
      eapply ~loc (evar ~loc f) [eapply ~loc cv1.e_of [evar ~loc x]]])
  ]) in
  let es_to, _, vs_to, _, _, e_of, acc =
    arrows_to_array ~options (ptyp_arrow ~loc Nolabel c1 c2) in
  let es_to = List.map (fun v -> eapply ~loc (jse ~loc "Unsafe.inject") [v]) es_to in
  let vs = List.map Ppx_js.Arg.name vs_to in
  let this_to, es_to = match es_to with [] -> assert false | h :: t -> h, t in
  let e_of =
    pexp_fun0 f (pexp_fun_rec (eapply ~loc e_of [
      eapply ~loc (jse ~loc "Unsafe.call") [
        evar ~loc f; this_to; arg_array ~loc es_to]])
      vs) in
  mconv ~acc:(acc @ cv1.e_acc @ cv2.e_acc) e_to e_of

and function_expr ~loc ~options (c1, c2) =
  match options.c_cb with
  | `cb -> callback_expr ~loc ~options (c1, c2)
  | `meth_cb -> meth_callback_expr ~loc ~options (c1, c2)
  | _ ->
    let options = reset_options options in
    let cv1 = expr_of_core ~options c1 in
    let cv2 = expr_of_core ~options c2 in
    let f, x = new_var (), new_var () in
    mconv ~acc:(cv1.e_acc @ cv2.e_acc)
      (pexp_fun0 f (pexp_fun0 x (eapply ~loc cv2.e_to [
         eapply ~loc (evar ~loc f) [eapply ~loc cv1.e_of [evar ~loc x]]])))
      (pexp_fun0 f (pexp_fun0 x (eapply ~loc cv2.e_of [
         eapply ~loc (evar ~loc f) [eapply ~loc cv1.e_to [evar ~loc x]]])))

and tuple_expr ~loc ~options = function
  | [] -> mconv [%expr fun _ -> ()] [%expr fun _ -> ()]
  | [h] -> expr_of_core ~options h
  | l ->
    let n = List.length l in
    let obj = new_var () in
    let fields_to, fields_of, acc = conv_map (fun i c ->
      let options = reset_options options in
      let cv = expr_of_core ~options c in
      let name_js = "_" ^ string_of_int i in
      mconv ~acc:cv.e_acc
        (Ppx_js.Val (mkl ~loc name_js, `Readonly, Fresh, eapply ~loc cv.e_to [
           eapply ~loc (i_project ~loc n i) [evar ~loc obj] ]))
        (eapply ~loc cv.e_of [prop_get ~loc (evar ~loc obj) name_js]))
      l in
    let fields_to = fields_to @ [
      Ppx_js.Val (mkl ~loc "length", `Readonly, Fresh, eint ~loc (List.length l))
    ] in
    mconv ~acc
      (pexp_fun0 obj @@ literal_object ~loc (pvar ~loc "_this") fields_to)
      (pexp_fun0 obj @@ pexp_tuple ~loc fields_of)

and array_tuple_expr ~loc ~options l =
  let es_to, es_of, acc = conv_map (fun _ c ->
    let options = reset_options options in
    expr_of_core ~options c) l in
  let e_to =
    let vs = List.init (List.length l) (fun _ -> new_var ()) in
    pexp_fun (ppat_tuple ~loc (List.map (pvar ~loc) vs)) @@
    eapply ~loc (jse ~loc "array") [
      pexp_array ~loc (List.map2 (fun v e_to ->
        eapply ~loc (jse ~loc "Unsafe.inject") [
          eapply ~loc e_to [evar ~loc v]]) vs es_to)] in
  let e_of =
    let a = new_var () in
    pexp_fun (pvar ~loc a) @@
    pexp_let ~loc Nonrecursive [
      value_binding ~loc ~pat:(pvar ~loc a)
        ~expr:(eapply ~loc (jse ~loc "to_array") [evar ~loc a])] @@
    pexp_tuple ~loc (List.mapi (fun i e_of ->
      eapply ~loc e_of [
        eapply ~loc (jse ~loc "Unsafe.coerce") [
          eapply ~loc (evar ~loc "Array.get") [ evar ~loc a; eint ~loc i ] ] ]) es_of) in
  mconv ~acc e_to e_of

and case_expr ~loc ?name_js ~options ~is_variant_case ?(kind=`Construct) ?(local=false) ?(case_option=false) ~add cv var : remember_item =
  let name_js = match name_js with None -> field_name ~case:is_variant_case options.cs_name | Some n -> n in
  let econstruct = match kind with
    | `Variant -> pexp_variant ~loc options.cs_name
    | `Construct -> pexp_construct ~loc (llid ~loc options.cs_name) in
  let pconstruct = match kind with
    | `Variant -> ppat_variant ~loc options.cs_name
    | `Construct -> ppat_construct ~loc (llid ~loc options.cs_name) in
  if not options.cs_singleton then
    let lhs_to =
      if local && case_option then
        ppat_construct ~loc (llid ~loc "Some") @@
        Some (ppat_alias ~loc (pconstruct (Option.map (fun _ -> ppat_any ~loc) var))
                (mkl ~loc (Option.get var)))
      else if local then
        ppat_alias ~loc (pconstruct (Option.map (fun _ -> ppat_any ~loc) var)) (mkl ~loc (Option.get var))
      else if case_option then ppat_construct ~loc (llid ~loc "Some") (Some (pconstruct (Option.map (pvar ~loc) var)))
      else pconstruct (Option.map (pvar ~loc) var) in
    let rhs_of =
      if case_option then pexp_construct ~loc (llid ~loc "Some") (Some (econstruct (Option.map (evar ~loc) var)))
      else econstruct (Option.map (evar ~loc) var) in
    let e_to obj = pexp_match ~loc obj [
      case ~guard:None
        ~lhs:lhs_to
        ~rhs:(eapply ~loc (jse ~loc "def") [
          eapply ~loc cv.e_to [
            Option.fold ~none:(eunit ~loc) ~some:(fun v -> evar ~loc v) var ] ]);
      case ~guard:None
        ~lhs:(ppat_any ~loc) ~rhs:(jse ~loc "undefined")
    ] in
    (* let rhs_of = match kind with *)
    (*   | `Variant -> *)
    (*     let g_type = ptyp_constr ~loc {txt=Lident options.g_name; loc} @@ List.map (fun _ -> ptyp_any ~loc) options.g_params in *)
    (*     pexp_coerce ~loc rhs_of None g_type *)
    (*   | _ -> rhs_of in *)
    let e_of obj expr =
      pexp_match ~loc (eapply ~loc (jse ~loc "Optdef.to_option") [
        prop_try ~loc obj name_js cv.e_of]) [
        case ~guard:None
          ~lhs:(psome ~loc (Option.fold ~none:ppat_any ~some:(fun v ~loc ->
            if local then ppat_construct ~loc
                (llid ~loc options.cs_name) (Some (pvar ~loc v))
            else pvar ~loc v) var))
          ~rhs:rhs_of;
        case ~guard:None ~lhs:(ppat_any ~loc) ~rhs:expr;
      ] in
    let e_to, e_of = match add with [] -> e_to, e_of | _ ->
      (fun obj -> pexp_let ~loc Nonrecursive add (e_to obj)),
      (fun obj expr -> pexp_let ~loc Nonrecursive add (e_of obj expr)) in
    if is_variant_case then
      mconv ~acc:cv.e_acc (`RCase (mkl ~loc name_js, e_to)) e_of
    else
      mconv ~acc:cv.e_acc (`RVal (mkl ~loc name_js, prop_kind options.f_prop, Fresh, e_to)) e_of
  else
    let e_to obj = match var with
      | None ->
        pexp_let ~loc Nonrecursive [
          value_binding ~loc ~pat:(ppat_any ~loc) ~expr:obj ] (eunit ~loc)
      | Some v ->
        if local then
          eapply ~loc cv.e_to [ obj ]
        else
          pexp_let ~loc Nonrecursive [
            value_binding ~loc
              ~pat:(pconstruct (Some (pvar ~loc v)))
              ~expr:obj ] (eapply ~loc cv.e_to [evar ~loc v]) in
    let e_of obj _ = match var with
      | None ->
        pexp_let ~loc Nonrecursive
          [ value_binding ~loc ~pat:(ppat_any ~loc) ~expr:obj ]
          (econstruct None)
      | Some _ ->
        if local then
          eapply ~loc cv.e_of [ prop_get ~loc obj name_js ]
        else
          econstruct (Some (eapply ~loc cv.e_of [ prop_get ~loc obj name_js ])) in
    mconv ~acc:cv.e_acc
      (`RVal (mkl ~loc name_js, prop_kind options.f_prop, Fresh, e_to)) e_of

and rest_case_expr ~loc ~name ~options ~is_variant_case ~add lid cv : remember_item =
  let var = new_var () in
  let lhs_to = ppat_alias ~loc (ppat_type ~loc {txt=lid; loc}) {txt=var; loc} in
  let e_to obj = pexp_match ~loc obj [
    case ~guard:None
      ~lhs:lhs_to
      ~rhs:(eapply ~loc (jse ~loc "def") [ eapply ~loc cv.e_to [ evar ~loc var ] ]);
    case ~guard:None
      ~lhs:(ppat_any ~loc) ~rhs:(jse ~loc "undefined")
  ] in
  let g_type = ptyp_constr ~loc {txt=Lident options.g_name; loc} @@ List.map (fun _ -> ptyp_any ~loc) options.g_params in
  let e_of obj expr =
    pexp_match ~loc (eapply ~loc (jse ~loc "Optdef.to_option") [
      prop_try ~loc obj name cv.e_of]) [
      case ~guard:None
        ~lhs:(psome ~loc (pvar var))
        ~rhs:(pexp_coerce ~loc (evar ~loc var) None g_type);
      case ~guard:None ~lhs:(ppat_any ~loc) ~rhs:expr;
    ] in
  let e_to, e_of = match add with [] -> e_to, e_of | _ ->
    (fun obj -> pexp_let ~loc Nonrecursive add (e_to obj)),
    (fun obj expr -> pexp_let ~loc Nonrecursive add (e_of obj expr)) in
  if is_variant_case then
    mconv ~acc:cv.e_acc (`RCase (mkl ~loc name, e_to)) e_of
  else
    mconv ~acc:cv.e_acc (`RVal (mkl ~loc name, prop_kind options.f_prop, Fresh, e_to)) e_of

and row_expr ?case_option ~options rf =
  let c_name = match options.c_variant with
    | `case c | `case_kind c ->
      let n = Option.value ~default:options.g_name c in
      (fun txt -> field_name ~case:true n ^ "_" ^ txt)
    | _ -> (fun txt -> field_name ~case:false txt) in
  let is_variant_case = is_variant_case options in
  let options = {(reset_options options) with cs_singleton = options.cs_singleton} in
  match rf.prf_desc with
  | Rtag ({txt; loc}, _, []) ->
    let options = field_attributes ~options:{options with f_key=txt; cs_name=txt} rf.prf_attributes in
    let cv = mconv (pexp_fun0 "_" (eunit ~loc)) (pexp_fun0 "_" (eunit ~loc)) in
    let name_js = c_name options.f_key in
    [ case_expr ~loc ~options ~is_variant_case ~name_js ~kind:`Variant ?case_option cv None ]
  | Rtag ({txt; loc}, _, (h :: _)) ->
    let options = field_attributes ~options:{options with f_key=txt; cs_name=txt} rf.prf_attributes in
    let name_js = c_name options.f_key in
    let cv = expr_of_core ~options h in
    [ case_expr ~loc ~is_variant_case ~name_js ~kind:`Variant ~options ?case_option cv
        (Some (new_var ())) ]
  | Rinherit ({ptyp_desc=Ptyp_constr ({txt; loc}, l); _} as c) ->
    let wrap = (field_attributes ~options c.ptyp_attributes).f_wrap in
    begin match wrap with
    | Some w ->
      let name = match w, txt with
        | Some n, _ -> c_name n
        | None, Lident n -> field_name ~case:is_variant_case (c_name n)
        | _ -> Location.raise_errorf ~loc:c.ptyp_loc "variant ident is not flat, wrap name is required" in
      let cv = expr_of_core ~options c in
      [ rest_case_expr ~loc ~is_variant_case ~name ~options txt cv ]
    | None ->
        match get_remember_fields (Longident.name txt) with
        | Some (fs, ps) ->
          let conv, _acc_expr = List.split @@ List.map2 (fun v c ->
            let cv = expr_of_core ~options c in
            let l, _ = add_expr0 ~options ~loc ~name:("_" ^ v) (cv.e_to, cv.e_of) in
            l, cv.e_acc)
            ps l in
          let param_exprs = List.flatten conv in
          let fs = List.map (fun f -> (fun ~add:_ -> f ~add:param_exprs)) fs in
          fs
        | _ -> []
    end
  | _ -> Location.raise_errorf ~loc:rf.prf_loc "Inherit type not handled"

and variant_expr0 ~loc ~options ?(case_option=false) l =
  let cs_singleton = List.length l = 1 in
  let options = {options with cs_singleton} in
  let l2 = List.fold_left (fun acc rf -> row_expr ~options ~case_option rf @ acc) [] l in
  remember_fields options.g_name (l2, List.map param_name (Jsoo_type.core_of_param options.g_params));
  let e_to, e_of, acc = conv_map (fun _ e -> e ~add:[]) l2 in
  let e_to = partition_rows e_to in
  let e_to = match options.c_variant with
    | `case_kind _ ->
      kind_field_to ~loc ~kind:`Variant ~case_option ~name:options.g_name e_to @@
      List.map (fun rf -> match rf.prf_desc with Rtag ({txt; _}, _, []) -> txt, false | Rtag ({txt; _}, _, _) -> txt, true | _ -> assert false) l
    | _ -> e_to in
  let e_of = List.rev e_of in
  let fail_or_none s =
    if case_option then pexp_construct ~loc (llid ~loc "None") None
    else eapply ~loc (evar ~loc "failwith") [ estring ~loc s ] in
  let rec aux obj = function
    | [] -> fail_or_none "no case matched"
    | h :: t -> h obj (aux obj t) in
  let aux_case_kind obj =
    pexp_match ~loc (prop_get ~loc obj ("__kind_"^options.g_name^"_")) @@
    List.map2 (fun rf field_of ->
      let lhs = match rf.prf_desc with Rtag ({txt; _}, _, _) -> pstring ~loc txt | _ -> assert false in
      let rhs = field_of obj (fail_or_none "field not found") in
      case ~guard:None ~rhs ~lhs
    ) l e_of @ [
      case ~guard:None ~rhs:(fail_or_none "field kind not recognized") ~lhs:(ppat_any ~loc)
    ] in
  mconv ~acc e_to (fun obj -> match options.c_variant with `case_kind _ -> aux_case_kind obj | _ -> aux obj e_of)

and variant_expr ~loc ~options ?case_option l =
  let cv = variant_expr0 ~loc ~options ?case_option l in
  let param_names = List.map (fun (c, _) -> param_name c) options.g_params in
  Hashtbl.add inherit_fields (lid options.g_name) (cv.e_to, param_names, None);
  mconv ~acc:cv.e_acc
    (pexp_fun1 ~loc (fun obj ->
       let expr = literal_object ~loc (pvar ~loc "_this") (cv.e_to obj) in
       if options.cs_singleton then expr else remove_undefined_expr ~loc expr))
    (pexp_fun1 ~loc cv.e_of)

and object_expr ~loc ~options l =
  let pexp_to ~loc obj lid = pexp_send ~loc obj {txt = Longident.name lid.txt; loc} in
  let l = List.map (fun pof ->
    let options = reset_options options in
    match pof.pof_desc with
    | Oinherit c ->
      let options = field_attributes ~options:{options with f_key=""; g_name=""; f_inherit=true} pof.pof_attributes in
      snd @@ field_expr ~loc ~options ~name_js:"" ~pexp_to c
    | Otag ({txt; loc}, c) ->
      let options = field_attributes ~options:{options with f_key=txt; g_name=txt} pof.pof_attributes in
      let name_js = field_name options.f_key in
      snd @@ field_expr ~loc ~options ~name_js ~pexp_to c
  ) l in
  let acc = List.flatten (List.map (fun c -> c.e_acc) l) in
  let e_to obj = List.flatten @@ List.map (fun c -> c.e_to obj) l in
  let param_names = List.map (fun (c, _) -> param_name c) options.g_params in
  let fields_of0 obj = List.fold_left (fun acc c ->
    let l = c.e_of obj in
    acc @ List.map (fun ({txt; _}, e) -> Longident.name txt, e) l) [] l in
  let fields_of obj = List.map (fun (txt, e) ->
    pcf_method ~loc ({txt; loc}, Public, Cfk_concrete (Fresh, e))) (fields_of0 obj) in
  Hashtbl.add inherit_fields (lid options.g_name) (e_to, param_names, Some fields_of0);
  mconv ~acc
    (pexp_fun1 ~loc (fun obj ->
       let e = literal_object ~loc (pvar ~loc "_this") (List.flatten @@ List.map (fun c -> c.e_to obj) l) in
       if options.f_rm_undefined then remove_undefined_expr ~loc e else e))
    (pexp_fun1 ~loc (fun obj ->
       pexp_object ~loc @@ class_structure ~self:(ppat_any ~loc) ~fields:(fields_of obj)))

and add_field_desc_params ~loc ~options ~names cs fd =
  match cs with
  | [] -> fd
  | cs ->
    let vb =
      List.map2 (fun c n ->
        let cv = expr_of_core ~options c in
        value_binding ~loc ~pat:(pvar ~loc @@ "_" ^ n ^ _to_jsoo)
          ~expr:cv.e_to) cs names in
    let aux e = pexp_let ~loc Nonrecursive vb e in
    match fd with
    | Ppx_js.Val (id, k, f, e) -> Ppx_js.Val (id, k, f, aux e)
    | Ppx_js.Meth (id, p, o, e, a) -> Ppx_js.Meth (id, p, o, aux e, a)
    | Ppx_js.Cases (ids, es) -> Ppx_js.Cases (ids, List.map aux es)

and field_expr ~loc ~name_js ~pexp_to ~options c =
  if not options.f_meth then match options.c_variant, c.ptyp_desc with
    | (`case _ | `case_kind _), Ptyp_variant (l, _, _) ->
      (* variant with cases  { a : [ `x | ... ] }*)
      let cv = variant_expr0 ~options ~loc l in
      let lid_name = llid ~loc options.g_name in
      let e_to obj = cv.e_to @@ pexp_to ~loc obj lid_name in
      let e_of obj = [lid_name, cv.e_of obj] in
      true, mconv ~acc:cv.e_acc e_to e_of
    | (`case _ | `case_kind _), Ptyp_constr ({txt = (Lident "option" | Ldot (Lident "Option", "t")); _}, [ {ptyp_desc = Ptyp_variant (l, _, _); _} ]) ->
      (* variant with cases  { a : [ `x | ... ] option }*)
      let cv = variant_expr0 ~options ~case_option:true ~loc l in
      let lid_name = llid ~loc options.g_name in
      let e_to obj = cv.e_to @@ pexp_to ~loc obj lid_name in
      let e_of obj = [lid_name, cv.e_of obj] in
      true, mconv ~acc:cv.e_acc e_to e_of
    | _, Ptyp_constr ({txt; loc}, cs) when options.f_inherit ->
      (* inherit  { a : ... [@inherit] } or < x: ...; a > *)
      let cv = expr_of_core ~options c in
      begin match Hashtbl.find_opt inherit_fields txt with
        | None -> Location.raise_errorf ~loc "inherited fields not available"
        | Some (f_to, param_names, f_of) ->
          let lid_name = llid ~loc options.g_name in
          false,
          mconv
            (fun obj ->
               List.map (add_field_desc_params ~loc ~options ~names:param_names cs) @@
               f_to @@ if options.g_name = "" then obj else pexp_to ~loc obj lid_name)
            (fun obj ->
               match options.g_name, f_of with
               | "", Some f_of -> List.map (fun (txt, e) -> llid ~loc txt, e) (f_of obj)
               | _ ->
                 [lid_name,
                  eapply ~loc cv.e_of [ eapply ~loc (jse ~loc "Unsafe.coerce") [obj] ] ])
      end
    | _ ->
      (* prop  { a : ... }*)
      let cv = match options.c_ignore, options.c_json with
        | true, _ -> mconv (id_fun loc) (id_fun loc)
        | _, Some enc ->
          let enc = Encoding.json_expr c enc in
          mconv
            [%expr fun x -> Js_json.js_of_json @@ Json_encoding.construct [%e enc] x]
            [%expr fun x -> Json_encoding.destruct [%e enc] (Js_json.json_of_js x)]
        | _ -> expr_of_core ~options c in
      let prop = match c.ptyp_desc with
        | Ptyp_constr ({txt; _}, _) when Longident.name txt = "ref" -> `mutable_
        | _ -> options.f_prop in
      let lid_name = llid ~loc options.g_name in
      false, mconv ~acc:cv.e_acc
        (fun obj -> [ Ppx_js.Val (mkl ~loc name_js, prop_kind prop, Fresh, eapply ~loc cv.e_to [
           pexp_to ~loc obj lid_name ]) ])
        (fun obj -> [lid_name, eapply ~loc cv.e_of [prop_get ~loc obj name_js]])
  else
    (* meth  { a : _ -> _ }*)
    let es_to, es_of, vs_to, vs_of, e_to, e_of, acc = arrows_to_array ~options ~meth_start:true c in
    let vs2_of = List.map Ppx_js.Arg.name vs_of in
    let vs2_to = List.map Ppx_js.Arg.name vs_to in
    let lid_name = llid ~loc options.g_name in
    false, mconv ~acc
      (fun obj -> [ Ppx_js.Meth (
         mkl ~loc name_js, Public, Fresh,
         pexp_fun_rec
           (eapply ~loc e_to [
              eapply ~loc (
                pexp_to ~loc obj lid_name)
                es_of ]) vs2_of, vs_of) ])
      (fun obj ->
         [lid_name,
          pexp_fun_rec (
            eapply ~loc e_of [
              method_call ~loc obj name_js
                (List.map (fun e_to -> Nolabel, e_to) es_to)]) vs2_to ])

let field_of_label_declaration ~options pld =
  let loc = pld.pld_loc in
  let g_name = pld.pld_name.txt in
  let f_key = remove_prefix g_name options.g_rm_prefix in
  let f_prop = match pld.pld_mutable with Mutable -> `mutable_ | _ -> options.f_prop in
  let options = { options with f_key; f_prop; g_name } in
  let options = field_attributes ~options pld.pld_attributes in
  let name_js = field_name options.f_key in
  field_expr ~loc ~name_js ~options ~pexp_to:pexp_field pld.pld_type

let record_expr ~loc ~options ?local l =
  let g_rm_prefix = prefix_length ~options @@ List.map (fun pld -> pld.pld_name.txt) l in
  let options = { options with g_rm_prefix } in
  let fields_to, fields_of, acc, _allow_overload =
    let l_to, l_of, l_acc, case = List.fold_left (fun (acc_to, acc_of, acc, acc_case) c ->
      let case, cv = field_of_label_declaration ~options c in
      (fun obj -> acc_to obj @ cv.e_to obj),
      (fun obj -> cv.e_of obj @ acc_of obj),
      cv.e_acc :: acc, case || acc_case)
      ((fun _obj -> []),(fun _obj -> []),[], false) l in
    l_to, l_of, List.flatten (List.rev l_acc), case in
  let param_names = List.map (fun (c, _) -> param_name c) options.g_params in
  Hashtbl.add inherit_fields (lid options.g_name) (fields_to, param_names, None);
  let e_to = match local with
    | None -> pexp_fun1 ~loc (fun obj ->
      let e = literal_object ~loc (pvar ~loc "_this") (fields_to obj) in
      if options.f_rm_undefined then remove_undefined_expr ~loc e
      else e)
    | Some cs ->
      let obj = new_var () in
      let e = literal_object ~loc (pvar ~loc "_this") (fields_to (evar ~loc obj)) in
      let rhs =
        if options.f_rm_undefined then remove_undefined_expr ~loc e
        else e in
      pexp_function ~loc ((
        case ~guard:None
          ~lhs:(ppat_construct ~loc (llid ~loc cs) (Some (pvar ~loc obj)))
          ~rhs) ::
        (if options.cs_singleton then []
         else
           [ case ~guard:None ~lhs:(ppat_any ~loc)
               ~rhs:(eapply ~loc (evar ~loc "failwith") [
                 estring ~loc "wrong local record construction"]) ])) in
  mconv ~acc
    e_to
    (match local with
     | None -> pexp_fun1 ~loc @@ fun obj -> pexp_record ~loc (List.rev @@ fields_of obj) None
     | Some cs -> pexp_fun1 ~loc @@ fun obj ->
       pexp_construct ~loc (llid ~loc cs)
         (Some (pexp_record ~loc (List.rev @@ fields_of obj) None)))

let field_of_constructor_declaration ~options pcd =
  let loc = pcd.pcd_loc in
  (match pcd.pcd_res with
   | Some _ -> Location.raise_errorf ~loc "GADT not handled"
   | None -> ());
  let name_cs = remove_prefix pcd.pcd_name.txt options.g_rm_prefix in
  let options = field_attributes ~options:{options with f_key=name_cs; cs_name=pcd.pcd_name.txt} pcd.pcd_attributes in
  let is_variant_case = is_variant_case options in
  let name_js = match options.c_variant with
    | `case c | `case_kind c ->
      let n = Option.value ~default:options.g_name c in
      field_name ~case:true (n ^ "_" ^ options.f_key)
    | _ -> field_name ~case:false options.f_key in
  let cv, var, local = match pcd.pcd_args with
    | Pcstr_tuple [] -> mconv (id_fun loc) (id_fun loc), None, false
    | Pcstr_tuple [c] ->
      let cv = expr_of_core ~options c in
      cv, Some (new_var ()), false
    | Pcstr_tuple l ->
      let var = if l = [] then None else Some (new_var ()) in
      if options.c_array then
        let cv = array_tuple_expr ~loc ~options l in
        cv, var, false
      else
        let cv = tuple_expr ~loc ~options l in
        let tuple_name = get_tuple_name options.g_name in
        let cv = acc_expr ~loc ~options ~name:tuple_name cv in
        cv, var, false
    | Pcstr_record l ->
      let name = "_" ^ pcd.pcd_name.txt ^ _jsoo in
      let options = { options with g_rm_prefix=`bool false } in
      let cv = record_expr ~loc ~options ~local:pcd.pcd_name.txt l in
      let cv = acc_expr ~loc ~options ~name cv in
      cv, Some (new_var ()), true in
  case_expr ~loc ~options ~is_variant_case ~name_js ~add:[] ~local cv var

let constructor_variant_expr ~loc ~options l =
  let cs_singleton = List.length l = 1 in
  let g_rm_prefix = prefix_length ~options @@ List.map (fun pcd -> pcd.pcd_name.txt) l in
  let options = { options with cs_singleton; g_rm_prefix } in
  let fields_to, fields_of, acc = conv_map (fun _ -> field_of_constructor_declaration ~options) l in
  let fields_to = partition_rows fields_to in
  let fields_to = match options.c_variant with
    | `case_kind _ ->
      kind_field_to ~loc ~kind:`Construct ~name:options.g_name fields_to @@
      List.map (fun pcd -> pcd.pcd_name.txt, (match pcd.pcd_args with Pcstr_tuple [] -> false | _ -> true)) l
    | _ -> fields_to in
  let param_names = List.map (fun (c, _) -> param_name c) options.g_params in
  Hashtbl.add inherit_fields (lid options.g_name) (fields_to, param_names, None);
  let rec aux obj = function
    | [] -> eapply ~loc (evar ~loc "failwith") [ estring ~loc "no case matched" ]
    | h :: t -> h obj (aux obj t) in
  let aux_case_kind obj =
    pexp_match ~loc (prop_get ~loc obj ("__kind_"^options.g_name^"_")) @@
    List.map2 (fun pcd field_of ->
      let lhs = pstring ~loc pcd.pcd_name.txt in
      let rhs = field_of obj (eapply ~loc (evar ~loc "failwith") [ estring ~loc "field not found" ]) in
      case ~guard:None ~rhs ~lhs
    ) l fields_of @ [
      case ~guard:None ~rhs:(eapply ~loc (evar ~loc "failwith") [ estring ~loc "field kind not recognized" ]) ~lhs:(ppat_any ~loc)
    ] in
  mconv ~acc
    (pexp_fun1 ~loc @@ fun obj ->
     let expr = literal_object ~loc (pvar ~loc "_this") (fields_to obj) in
     if cs_singleton then expr else remove_undefined_expr ~loc expr)
    (pexp_fun1 ~loc @@ fun obj ->
     match options.c_variant with `case_kind _ -> aux_case_kind obj | _ -> aux obj fields_of)

let declaration_of_manifest ~options c =
  let loc = c.ptyp_loc in
  let options = core_attributes ~options c.ptyp_attributes in
  match options.c_conv, c.ptyp_desc with
  | Some c, _ -> c
  | _, Ptyp_tuple l ->
    if options.c_array then array_tuple_expr ~loc ~options l
    else tuple_expr ~loc ~options l
  | _, Ptyp_variant (l, _, _) ->
    begin match options.c_variant with
      | `string_enum | `int_enum as typ ->
        let ls, fallback = enum_variant ~options typ l in
        enum_expr ~loc ?fallback typ ls
      | _ ->
        if is_variant_enum (`variant l) then
          let ls, fallback = enum_variant ~options `string_enum l in
          enum_expr ~loc ?fallback `string_enum ls
        else variant_expr ~loc ~options l
    end
  | _ -> expr_of_core ~options c

let declaration_of_type_kind ~loc ~options kind manifest =
  match kind, manifest with
  | Ptype_abstract, None -> Location.raise_errorf ~loc "abstract type"
  | Ptype_open, _ -> Location.raise_errorf ~loc "open type"
  | Ptype_abstract, Some manifest -> declaration_of_manifest ~options:{options with g_rm_prefix=0} manifest
  | Ptype_record l, _ -> record_expr ~loc ~options l
  | Ptype_variant l, _ ->
    match options.c_variant with
    | `string_enum | `int_enum as typ ->
      let ls, fallback = enum_constructor ~options typ l in
      enum_expr ~loc ?fallback ~kind:`Construct typ ls
    | _ ->
      if is_variant_enum (`cons l) then
        let ls, fallback = enum_constructor ~options `string_enum l in
        enum_expr ~loc ?fallback ~kind:`Construct `string_enum ls
      else constructor_variant_expr ~loc ~options l

let conv_expressions ~options t =
  let loc = t.ptype_loc in
  let options = { options with g_name=t.ptype_name.txt; g_params=t.ptype_params } in
  let cv = declaration_of_type_kind ~loc ~options t.ptype_kind t.ptype_manifest in
  mconv ~acc:cv.e_acc (add_params_fun cv.e_to options.g_params) (add_params_fun cv.e_of options.g_params)

let conv_signatures ~options ~is_class_type t =
  let loc = t.ptype_loc in
  let g_name = t.ptype_name.txt in
  let g_params = t.ptype_params in
  let options = { options with g_name; g_params } in
  let name_js = jsoo_name ~options g_name in
  let rec aux = function
    | [] -> (fun expr -> expr), []
    | ({ptyp_desc = Ptyp_var x; _}, _) :: t ->
      let t_to = ptyp_arrow ~loc Nolabel (ptyp_var ~loc x) (ptyp_var ~loc (x ^ _jsoo)) in
      let t_of = ptyp_arrow ~loc Nolabel (ptyp_var ~loc (x ^ _jsoo)) (ptyp_var ~loc x) in
      let c, vs = aux t in
      (fun expr -> ptyp_arrow ~loc Nolabel (ptyp_tuple ~loc [t_to; t_of]) (c expr)),
      x :: vs
    | _ :: t -> aux t in
  let c, vars = aux g_params in
  let ml_vars = List.map (fun v -> ptyp_var ~loc v) vars in
  let js_vars = List.map (fun v -> ptyp_var ~loc (v ^ _jsoo)) vars in
  let jsoo_ct_sig = ptyp_constr ~loc (llid ~loc name_js) js_vars in
  let jsoo_sig =
    if is_class_type then ptyp_constr ~loc (llid ~loc (js_mod "t")) [jsoo_ct_sig]
    else jsoo_ct_sig in
  let ml_sig = ptyp_constr ~loc (mkl ~loc @@ Longident.parse g_name) ml_vars in
  let to_sig = c @@ ptyp_arrow ~loc Nolabel ml_sig jsoo_sig in
  let of_sig = c @@ ptyp_arrow ~loc Nolabel jsoo_sig ml_sig in
  to_sig, of_sig, ptyp_tuple ~loc [to_sig; of_sig]
