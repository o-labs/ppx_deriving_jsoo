open Ppxlib
open Ast_builder.Default
open Common

let core_of_param = List.map fst
let core_tail ptyp = match ptyp.ptyp_desc with
  | Ptyp_constr (_, l) -> l
  | _ -> Location.raise_errorf ~loc:ptyp.ptyp_loc "core type is not a constr"

let typ_constr ~loc s l = ptyp_constr ~loc (mkl ~loc (Longident.parse s)) l
let ctyp_constr ~loc s l = pcty_constr ~loc (mkl ~loc (Longident.parse s)) l

let prop_kind = function
  | `readonly -> "readonly_prop"
  | `writeonly -> "writeonly_prop"
  | `case -> "case_prop"
  | `mutable_ -> "prop"

let rec typ_constr_list ~loc = function
  | [] -> assert false
  | [h] -> typ_constr ~loc h []
  | h :: t -> typ_constr ~loc h [typ_constr_list ~loc t]

let ml_type_to_class_type ~options s l = match s, l with
  | "unit", _ -> "unit"
  | "int", [] | "int32", [] | "Int32.t", [] | "Int.t", [] ->
    if options.c_number then js_mod "number" else "int"
  | "int64", [] | "Int64.t", [] | "nativeint", [] | "Nativeint.t", [] ->
    if !bigint then js_mod "bigInt" else js_mod "number"
  | "Z.t", [] ->
    if !bigint then js_mod "bigInt" else js_mod "js_string"
  | "float", [] | "Float.t", [] -> js_mod "number"
  | "string", [] | "String.t", [] | "char", [] | "Char.t", [] | "Hex.t", []
    -> js_mod "js_string"
  | "bytes", [] | "Bytes.t", [] | "Bigstring.t", [] | "Cstruct.buffer", []
    -> jsoo_mod "Typed_array.arrayBuffer"
  | "list", [_] | "array", [_] | "List.t", [_] | "Array.t", [_] -> js_mod "js_array"
  | "option", [_] | "Option.t", [_] ->
    js_mod (match options.c_opt with `opt -> "opt" | `optdef -> "optdef" | `aopt -> "aopt")
  | "Unsafe.any", [] | "any", [] | "Js.Unsafe.any", [] | "Js_of_ocaml.Js.Unsafe.any", []
    -> js_mod "Unsafe.any"
  | s, [] when s = js_mod "Unsafe.any" -> js_mod "Unsafe.any"
  | "bool", [] | "Bool.t", [] -> "bool"
  | s, _ -> jsoo_name ~options s

let prop ~loc core = typ_constr ~loc (js_mod "prop") [core]
let readonly_prop ~loc core = typ_constr ~loc (js_mod "readonly_prop") [core]
let case_prop ~loc core = typ_constr ~loc (js_mod "case_prop") [core]
let meth ~loc core = typ_constr ~loc (js_mod "meth") [core]

let js_monad ?(inherit_=false) ~loc = function
  | TT c -> c
  | CT c ->
    if inherit_ then c
    else typ_constr ~loc (js_mod "t") [c]

let fold_ctt ~options ~ctt ~tt name core = match name with
  | "int"  | "int32" | "Int.t" | "Int32.t" when not options.c_number -> TT (tt core)
  | "unit" | "option" | "any" | "Unsafe.any" | "Js.Unsafe.any"
  | "Js_of_ocaml.Js.Unsafe.any" | "meth" -> TT (tt core)
  | s when s = js_mod "Unsafe.any" -> TT (tt core)
  | _ -> CT (ctt core)

let mk_ctt_core ~options name core = fold_ctt ~options ~ctt:(fun x -> x) ~tt:(fun x -> x) name core

let map_acc f g end_ start l =
  let l, acc = List.fold_left (fun (l, acc) x ->
    let x, acc2 = f x in
    g l x, acc @ acc2) (start, []) l in
  List.rev (end_ l), acc

let map_acc0 f l =
  let l, acc = List.fold_left (fun (l, acc) x ->
    let x, acc2 = f x in
    x :: l, acc @ acc2) ([], []) l in
  List.rev l, acc

let typ_to_ctyp = function
  | CT {ptyp_desc=Ptyp_constr (lloc, l); ptyp_loc; _} -> pcty_constr ~loc:ptyp_loc lloc l
  | TT {ptyp_loc; _} | CT {ptyp_loc; _} ->
    Location.raise_errorf ~loc:ptyp_loc "cannot translate this core type to class type constr"

let assoc_type ~options name l =
  if not options.c_assoc then None
  else if name = "list" || name = "List.t" then
    match l with
    | [ {ptyp_desc = Ptyp_tuple [ c1; c2 ]; _} ] ->
      begin match c1.ptyp_desc with
        | Ptyp_constr ({txt; _}, []) ->
          let cons = Longident.name txt in
          if cons = "string" || cons = "String.t" then Some c2
          else None
        | _ -> None
      end
    | _ -> None
  else None

let kind_field ~loc ~name =
  pctf_method ~loc (mkl ~loc ("__kind_"^name^"_"), Public, Concrete, readonly_prop ~loc @@ ptyp_constr ~loc (llid ~loc "string") [])

let rec type_of_core ~options c =
  let loc = c.ptyp_loc in
  let options = core_attributes ~options c.ptyp_attributes in
  match options.c_ignore, options.c_type, options.c_json with
  | true, _, _ -> TT c, []
  | _, Some (TT t), _ -> TT (ptyp_constr ~loc t []), []
  | _, Some (CT ct), _ -> CT (ptyp_constr ~loc ct []), []
  | _, _, Some _ -> TT (typ_constr ~loc (js_mod "Unsafe.any") []), []
  | _ -> match c.ptyp_desc with
    | Ptyp_any -> TT (ptyp_any ~loc), []
    | Ptyp_var v ->
      let c = TT (ptyp_var ~loc v) in
      if options.f_meth then TT (meth ~loc (js_monad ~loc c)), []
      else c, []
    | Ptyp_tuple [] -> TT (typ_constr ~loc "unit" []), []
    | Ptyp_tuple [h] -> type_of_core ~options h
    | Ptyp_tuple l ->
      if options.c_array then
        TT (typ_constr_list ~loc [js_mod "t"; js_mod "js_array"; js_mod "Unsafe.any"]), []
      else
        let expr, acc = tuple_type ~loc ~options l in
        let tuple_name = get_tuple_name options.g_name in
        let ct = class_infos ~loc
            ~virt:Concrete ~params:options.g_params ~name:(mkl ~loc tuple_name) ~expr in
        CT (typ_constr ~loc tuple_name (core_of_param options.g_params)), acc @ [ ct ]
    | Ptyp_arrow (_a, c1, c2) -> arrow_type ~loc ~options (c1, c2)
    | Ptyp_object (l, _) ->
      let expr, acc = object_type ~loc ~options l in
      let object_name = get_object_name options.g_name in
      let ct = class_infos ~loc
          ~virt:Concrete ~params:options.g_params ~name:(mkl ~loc object_name) ~expr in
      let c, acc =
        CT (typ_constr ~loc object_name (core_of_param options.g_params)), acc @ [ ct ] in
      if options.f_meth then TT (meth ~loc (js_monad ~loc c)), acc
      else c, acc
    | Ptyp_variant (l, _, _) ->
      begin match options.c_variant with
        | `string_enum -> CT (typ_constr ~loc (js_mod "js_string") []), []
        | `int_enum -> CT (typ_constr ~loc (js_mod "number") []), []
        | _ ->
          if is_variant_enum (`variant l) then CT (typ_constr ~loc (js_mod "js_string") []), []
          else
            let expr, acc = variant_type ~loc ~options l in
            let variant_name = get_variant_name options.g_name in
            let ct = class_infos ~loc
                ~virt:Concrete ~params:options.g_params ~name:(mkl ~loc variant_name) ~expr in
            CT (typ_constr ~loc variant_name (core_of_param options.g_params)), acc @ [ ct ]
      end
    | Ptyp_constr ({txt; _}, [c]) when Longident.name txt = "ref" || Longident.name txt = "Lazy.t" ->
      type_of_core ~options:{ options with f_prop = `mutable_ } c
    | Ptyp_constr ({txt; _}, l) ->
      let type_name = Longident.name txt in
      begin match assoc_type ~options type_name l with
        | None ->
          let c, acc = map_acc0 (monad_of_core ~options:(reset_options options)) l in
          let c = mk_ctt_core ~options type_name @@
            typ_constr ~loc (ml_type_to_class_type ~options type_name c) c in
          if options.f_meth then TT (meth ~loc (js_monad ~loc c)), acc
          else c, acc
        | Some c ->
          let c, acc = monad_of_core ~options:(reset_options options) c in
          TT (typ_constr ~loc (js_mod "Table.t") [ c ]), acc
      end
    | _ -> Location.raise_errorf ~loc "type not handled"

and monad_of_core ~options c =
  let loc = c.ptyp_loc in
  let c, acc = type_of_core ~options c in
  js_monad ~inherit_:options.f_inherit ~loc c, acc

and tuple_type ~loc ~options l =
  let fields, acc =
    map_acc (monad_of_core ~options:(reset_options options))
      (fun (fields, i) c ->
         let f = pctf_method ~loc (
           (mkl ~loc @@ "_" ^ string_of_int i), Public, Concrete,
           readonly_prop ~loc c) in
         f :: fields, i+1) fst ([], 0) l in
  let fields = fields @ [
    pctf_method ~loc (mkl ~loc "length", Public, Concrete, readonly_prop ~loc @@ ptyp_constr ~loc (llid ~loc "int") [])
  ] in
  pcty_signature ~loc @@ class_signature ~self:(ptyp_any ~loc) ~fields, acc

and arrow_type ~loc ~options (c1, c2) =
  let c2, acc2 = monad_of_core ~options:{(reset_options options) with f_meth = options.f_meth; g_meth_start=false} c2 in
  match options.g_meth_start, options.c_cb, c1 with
  | true, `fun_, {ptyp_desc = Ptyp_constr ({txt; _}, _); _} when Longident.name txt = "unit" ->
    TT c2, acc2
  | _, `meth_cb, _ ->
    let c1, acc1 = monad_of_core ~options:(reset_options options) c1 in
    TT (typ_constr ~loc (js_mod "meth_callback") [c1; c2]), acc1 @ acc2
  | _, cb, _ ->
    let c1, acc1 = monad_of_core ~options:(reset_options options) c1 in
    let c = ptyp_arrow ~loc Nolabel c1 c2 in
    let c = match cb with `cb -> typ_constr ~loc (js_mod "callback") [c] | _ -> c in
    TT c, acc1 @ acc2

and variant_type ~loc ~options l =
  let c_name, c_prop = match (List.length l = 1), options.c_variant with
    | true, _ -> Fun.id, readonly_prop
    | _, (`case c | `case_kind c) ->
      let n = Option.value ~default:options.g_name c in
      (fun txt -> n ^ "_" ^ txt), case_prop
    | _ -> Fun.id, (fun ~loc c -> readonly_prop ~loc (typ_constr ~loc (js_mod "optdef") [c])) in
  let is_variant_case = is_variant_case options in
  let fields, acc = map_acc0 (fun rf ->
    let options = reset_options options in
    match rf.prf_desc with
    | Rtag ({txt; loc}, _, []) ->
      let options = field_attributes ~options:{options with f_key=txt} rf.prf_attributes in
      pctf_method ~loc (
        mkl ~loc (field_name ~case:is_variant_case (c_name options.f_key)), Public, Concrete,
        c_prop ~loc (typ_constr ~loc "unit" [])), []
    | Rtag ({txt; loc}, _, (h :: _)) ->
      let options = field_attributes ~options:{options with f_key=txt} rf.prf_attributes in
      let c, acc = monad_of_core ~options h in
      pctf_method ~loc (
        mkl ~loc (field_name ~case:is_variant_case (c_name options.f_key)), Public, Concrete,
        c_prop ~loc c), acc
    | Rinherit c ->
      let loc = c.ptyp_loc in
      let wrap = (field_attributes ~options c.ptyp_attributes).f_wrap in
      match wrap with
      | None ->
        let c, acc = type_of_core ~options:(reset_options options) c in
        pctf_inherit ~loc (typ_to_ctyp c), acc
      | Some w ->
        let name = match w, c.ptyp_desc with
          | Some n, _ -> c_name n
          | None, Ptyp_constr ({txt=Lident n; _}, _) -> field_name ~case:is_variant_case (c_name n)
          | _ -> Location.raise_errorf ~loc "variant ident is not flat, wrap name is required" in
        let c, acc = monad_of_core ~options c in
        pctf_method ~loc ((mkl ~loc name), Public, Concrete, c_prop ~loc c), acc
  ) l in
  let fields = match options.c_variant with `case_kind _ -> kind_field ~loc ~name:options.g_name :: fields | _ -> fields in
  pcty_signature ~loc @@ class_signature ~self:(ptyp_any ~loc) ~fields, acc

and object_type ~loc ~options l =
  let fields, acc =
    map_acc0 (fun pof ->
      let options = reset_options options in
      match pof.pof_desc with
      | Otag ({txt; loc}, c) ->
        let options = field_attributes ~options:{options with f_key=txt; g_name=txt} pof.pof_attributes in
        if not options.f_inherit then
          let name_js = field_name options.f_key in
          let c, acc = if options.c_ignore then c, [] else monad_of_core ~options c in
          let prop = match c.ptyp_desc, options.f_meth with
            | Ptyp_constr ({txt; _}, _), false when Longident.name txt = "ref" -> Some "prop"
            | _, true -> None
            | _ -> Some (prop_kind options.f_prop) in
          let c = match prop with None -> c | Some prop -> typ_constr ~loc (js_mod prop) [c] in
          pctf_method ~loc (mkl ~loc name_js, Public, Concrete, c), acc
        else
          let c, acc = type_of_core ~options c in
          pctf_inherit ~loc (typ_to_ctyp c), acc
      | Oinherit c ->
        let loc = c.ptyp_loc in
        let c, acc = type_of_core ~options c in
        pctf_inherit ~loc (typ_to_ctyp c), acc) l in
  pcty_signature ~loc @@ class_signature ~self:(ptyp_any ~loc) ~fields, acc

let field_of_label_declaration ~options pld =
  let loc = pld.pld_loc in
  let f_key = remove_prefix pld.pld_name.txt options.g_rm_prefix in
  let options = field_attributes ~options:{options with f_key; g_name=pld.pld_name.txt} pld.pld_attributes in
  let name_js = field_name options.f_key in
  match options.c_variant, pld.pld_type.ptyp_desc with
  | (`case _ | `case_kind _), (Ptyp_variant (l, _, _) | Ptyp_constr ({txt=(Lident "option" | Ldot (Lident "Option", "t")); _}, [ {ptyp_desc=Ptyp_variant (l, _, _); _} ])) ->
    let expr, acc = variant_type ~loc ~options l in
    let variant_name = get_variant_name options.g_name in
    let ct = class_infos ~loc ~virt:Concrete ~params:options.g_params ~name:(mkl ~loc variant_name) ~expr in
    pctf_inherit ~loc
      (pcty_constr ~loc (llid ~loc variant_name)
         (core_of_param options.g_params)), acc @ [ ct ]
  | _ ->
    let c, acc = match options.c_ignore, options.c_json with
      | true , _ -> pld.pld_type, []
      | _, Some _ -> typ_constr ~loc (js_mod "Unsafe.any") [], []
      | _ -> monad_of_core ~options pld.pld_type in
    if not options.f_inherit then
      let prop = match pld.pld_type.ptyp_desc, pld.pld_mutable, options.f_meth with
        | _, _, true -> None
        | Ptyp_constr ({txt; _}, _), _, _ when Longident.name txt = "ref" -> Some "prop"
        | _, Mutable, _ -> Some "prop"
        | _ -> Some (prop_kind options.f_prop) in
      let c = match prop with None -> c | Some prop -> typ_constr ~loc (js_mod prop) [c] in
      pctf_method ~loc (mkl ~loc name_js, Public, Concrete, c), acc
    else
      pctf_inherit ~loc (typ_to_ctyp (CT c)), acc

let field_of_constructor_declaration ~options pcd =
  let loc = pcd.pcd_loc in
  (match pcd.pcd_res with
   | Some _ -> Location.raise_errorf ~loc "GADT not handled"
   | None -> ());
  let name_cs = remove_prefix pcd.pcd_name.txt options.g_rm_prefix in
  let default = options.g_name in
  let options = field_attributes ~options:{ options with f_key = name_cs; g_name=pcd.pcd_name.txt } pcd.pcd_attributes in
  let name_js = match options.c_variant with
    | `case c | `case_kind c ->
      let n = Option.value ~default c in
      field_name ~case:true (n ^ "_" ^ options.f_key)
    | _ -> field_name ~case:false options.f_key in
  let desc, record = match pcd.pcd_args with
    | Pcstr_tuple l -> type_of_core ~options (ptyp_tuple ~loc l)
    | Pcstr_record l ->
      let fields, acc = map_acc0 (field_of_label_declaration ~options:(reset_options options)) l in
      let expr = pcty_signature ~loc @@ class_signature ~self:(ptyp_any ~loc) ~fields in
      let tuple_name = get_record_name options.g_name in
      let ct = class_infos ~loc
          ~virt:Concrete ~params:options.g_params ~name:(mkl ~loc tuple_name) ~expr in
      CT (typ_constr ~loc tuple_name (core_of_param options.g_params)), acc @ [ ct ] in
  let c = js_monad ~loc desc in
  let c = match options.c_variant, options.cs_singleton with
    | (`case _ | `case_kind _), _ -> case_prop ~loc c
    | _, true -> typ_constr ~loc (js_mod (prop_kind options.f_prop)) [ c ]
    | _ ->
      typ_constr ~loc (js_mod (prop_kind options.f_prop)) [typ_constr ~loc (js_mod "optdef") [c]] in
  pctf_method ~loc (mkl ~loc name_js, Public, Concrete, c), record

let rec declaration_of_manifest ~options c =
  let loc = c.ptyp_loc in
  let options = core_attributes ~options c.ptyp_attributes in
  match options.c_ignore, options.c_type with
  | true, _ -> TT c, []
  | _, Some (TT t) -> TT (ptyp_constr ~loc t []), []
  | _, Some (CT ct) -> CT (pcty_constr ~loc ct []), []
  | _ ->
    match c.ptyp_desc with
    | Ptyp_constr ({txt; _}, [c]) when Longident.name txt = "ref" || Longident.name txt = "Lazy.t"  ->
      declaration_of_manifest ~options c
    | Ptyp_constr ({txt=Lident "bool"; _}, _l)
    | Ptyp_constr ({txt=Ldot (Lident "Bool", "t"); _}, _l) ->
      TT (typ_constr_list ~loc [ js_mod "t"; "bool"]), []
    | Ptyp_constr ({txt; _}, l) ->
      let name = Longident.name txt in
      let s_js = ml_type_to_class_type ~options name l in
      let fields, acc = map_acc0 (monad_of_core ~options:(reset_options options)) l in
      fold_ctt ~options name fields
        ~ctt:(fun fields -> ctyp_constr ~loc s_js fields)
        ~tt:(fun fields -> typ_constr ~loc s_js fields), acc
    | Ptyp_tuple [] -> TT (typ_constr ~loc "unit" []), []
    | Ptyp_tuple [h] -> declaration_of_manifest ~options h
    | Ptyp_tuple l ->
      if options.c_array then
        CT (ctyp_constr ~loc (js_mod "js_array") [typ_constr ~loc (js_mod "Unsafe.any") []]), []
      else
        let c, acc = tuple_type ~loc ~options l in
        CT c, acc
    | Ptyp_arrow _ ->
      let c, acc = monad_of_core ~options c in
      TT c, acc
    | Ptyp_variant (l, _, _) ->
      begin match options.c_variant with
        | `string_enum -> CT (ctyp_constr ~loc (js_mod "js_string") []), []
        | `int_enum -> CT (ctyp_constr ~loc (js_mod "number") []), []
        | _ ->
          if is_variant_enum (`variant l) then
            CT (ctyp_constr ~loc (js_mod "js_string") []), []
          else
            let c, acc = variant_type ~loc ~options l in
            CT c, acc
      end
    | Ptyp_object (l, _) ->
      let c, acc = object_type ~loc ~options l in
      CT c, acc
    | _ -> Location.raise_errorf ~loc "cannot derive jsoo of this manifest"

let declaration_of_type_kind ~options t =
  let loc = t.ptype_loc in
  let g_name, g_params = t.ptype_name.txt, t.ptype_params in
  let options = { options with g_name; g_params } in
  match t.ptype_kind, t.ptype_manifest with
  | Ptype_abstract, None -> Location.raise_errorf ~loc "cannot derive jsoo of abstract type"
  | Ptype_abstract, Some manifest -> declaration_of_manifest ~options:{options with g_rm_prefix=0} manifest
  | Ptype_open, _ -> Location.raise_errorf ~loc "cannot derive jsoo of open type"
  | Ptype_record l, _ ->
    let g_rm_prefix = prefix_length ~options @@ List.map (fun pld -> pld.pld_name.txt) l in
    let fields, acc =
      map_acc0 (field_of_label_declaration ~options:{ options with g_rm_prefix }) l in
    CT (pcty_signature ~loc @@ class_signature ~self:(ptyp_any ~loc) ~fields), acc
  | Ptype_variant l, _ ->
    match options.c_variant with
    | `string_enum -> CT (ctyp_constr ~loc (js_mod "js_string") []), []
    | `int_enum -> CT (ctyp_constr ~loc (js_mod "number") []), []
    | _ ->
      if is_variant_enum (`cons l) then
        CT (ctyp_constr ~loc (js_mod "js_string") []), []
      else
        let g_rm_prefix = prefix_length ~options @@ List.map (fun pcd -> pcd.pcd_name.txt) l in
        let cs_singleton = List.length l = 1 in
        let fields, acc =
          map_acc0 (field_of_constructor_declaration ~options:{ options with g_rm_prefix; cs_singleton }) l in
        let fields = match options.c_variant with `case_kind _ -> kind_field ~loc ~name:options.g_name :: fields | _ -> fields in
        CT (pcty_signature ~loc @@ class_signature ~self:(ptyp_any ~loc) ~fields), acc
