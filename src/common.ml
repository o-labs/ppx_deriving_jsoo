open Ppxlib
module SMap = Map.Make(String)

type ('a, 'b) conv0 = {e_to : 'a; e_of : 'b; e_acc : (conv * (string * string)) list}
and conv = (expression, expression) conv0

type ('a, 'b) ctt =
  | CT of 'a
  | TT of 'b

let verbose = match Sys.getenv_opt "PPX_JSOO_DEBUG" with
  | None | Some "0" | Some "false" | Some "no" -> ref 0
  | Some s ->
    match s with
    | "true" -> Format.printf "ppx_jsoo_debug activated\n@."; ref 1
    | s -> match int_of_string_opt s with
      | Some i -> Format.printf "ppx_jsoo_debug activated\n@."; ref i
      | None -> ref 0

let fake = match Sys.getenv_opt "PPX_JSOO_FAKE" with
  | Some "1" | Some "true" | Some "yes" -> ref true
  | _ -> ref false

let bigint = match Sys.getenv_opt "PPX_JSOO_BIGINT" with
  | Some "0" | Some "false" | Some "no" -> ref false
  | _ -> ref true

let debug ?(v=1) ?(force=false) fmt =
  if !verbose >= v || force then Format.ksprintf (fun s -> Format.eprintf "%s@." s) fmt
  else Printf.ifprintf () fmt

let jsoo = "jsoo"
let _jsoo = "_jsoo"
let to_jsoo = "to" ^ _jsoo
let of_jsoo = "of" ^ _jsoo
let jsoo_conv = jsoo ^ "_conv"
let _jsoo_conv = _jsoo ^ "_conv"
let _to_jsoo = "_" ^ to_jsoo
let _of_jsoo = "_" ^ of_jsoo
let tuple_jsoo = "_tupjsoo"
let jsoo_mod s = "Ezjs_min." ^ s
let js_mod s = jsoo_mod s

let mkl ~loc txt = { Asttypes.txt; loc }
let mknol txt = mkl ~loc:!Ast_helper.default_loc txt

let lid s = Longident.parse s
let llid ~loc s = mkl ~loc (lid s)

type rm_prefix = [ `bool of bool | `length of int | `prefix of string ]
type variant_kind = [ `case of string option | `case_kind of string option | `string_enum | `int_enum | `default ]

type 'rm options = {
  c_opt: [`optdef | `opt | `aopt];
  c_ignore: bool;
  c_cb: [`cb | `meth_cb | `fun_];
  c_array : bool;
  c_assoc: bool;
  c_number: bool;
  c_variant: variant_kind;
  c_type: (longident_loc, longident_loc) ctt option;
  c_conv: conv option;
  c_json: expression option option;
  f_key: string;
  f_prop: [`readonly | `writeonly | `mutable_ | `case ];
  f_inherit: bool;
  f_code: int option;
  f_case: [ `camel | `snake | `default ];
  f_meth: bool;
  f_rm_undefined: bool;
  f_wrap: string option option;
  cs_singleton: bool;
  cs_name: string;
  g_meth_start: bool;
  g_modules: (string * string) list;
  g_name: string;
  g_params: (core_type * (variance * injectivity)) list;
  g_rm_prefix: 'rm;
}

let default_options : rm_prefix options = {
  f_key=""; f_prop=`readonly; f_inherit=false; f_code=None; f_case=`default;
  f_meth=false; f_rm_undefined=false; f_wrap=None;
  c_opt=`optdef; c_ignore=false; c_cb=`fun_; c_array=false; c_assoc=false;
  c_number=false; c_variant=`default; c_type=None; c_conv=None; c_json=None;
  cs_singleton=false; cs_name=""; g_modules=[]; g_name=""; g_params=[];
  g_meth_start=true; g_rm_prefix=`bool true;
}

let reset_options options = {
  default_options with
  g_modules=options.g_modules; g_name=options.g_name;
  g_params=options.g_params; g_rm_prefix=0;
  f_case=options.f_case
}

let is_variant_case o = match o.c_variant with `case _ | `case_kind _ -> true | _ -> false

let jsoo_name ?(suff=jsoo) ~options name =
  let rec aux acc = function
    | [] -> assert false
    | [ "t" ] -> acc ^ suff
    | [ s ] -> acc ^ s ^ "_" ^ suff
    | h :: t ->
      match List.assoc_opt h options.g_modules with
      | None -> aux (acc ^ h ^ ".") t
      | Some h -> aux (acc ^ h ^ ".") t in
  aux "" (String.split_on_char '.' name)

let jsoo_name_to ~options name = jsoo_name ~options ~suff:to_jsoo name
let jsoo_name_of ~options name = jsoo_name ~options ~suff:of_jsoo name
let jsoo_name_conv ~options name = jsoo_name ~options ~suff:jsoo_conv name

let new_var = let i = ref (-1) in fun () -> incr i; "v" ^ string_of_int !i

let get_expr_attr = function
  | PStr [{pstr_desc = Pstr_eval (e, _); _}] -> Some e
  | _ -> None

let get_string_attr = function
  | PStr [{pstr_desc = Pstr_eval ({pexp_desc = Pexp_constant (Pconst_string (s, _, _)); _}, _); _}] ->
    Some s
  | _ -> None

let get_int_attr = function
  | PStr [{pstr_desc = Pstr_eval ({pexp_desc = Pexp_constant (Pconst_integer (s, _)); _}, _); _}] ->
    int_of_string_opt s
  | _ -> None

let get_ident_attr = function
  | PStr [{pstr_desc = Pstr_eval ({pexp_desc = Pexp_ident s; _}, _); _}] ->
    Some s
  | _ -> None

let get_enum_attr = function
  | PStr [{pstr_desc = Pstr_eval ({pexp_desc = Pexp_construct (lid, _); _}, _); _}] ->
    begin match lid.txt with
      | Lident "String" -> `string_enum
      | Lident "Int" -> `int_enum
      | _ -> `string_enum
    end
  | _ -> `string_enum

let get_conv_attr = function
  | PStr [{pstr_desc = Pstr_eval ({pexp_desc = Pexp_tuple [e_to; e_of]; _}, _); _}] ->
    Some {e_of = Ppx_js.transform#expression e_of; e_to = Ppx_js.transform#expression e_to; e_acc=[]}
  | PStr [{pstr_desc = Pstr_eval (e, _); _}] ->
    let e = Ppx_js.transform#expression e in
    let loc = e.pexp_loc in
    let e_to, e_of = Ast_builder.Default.(eapply ~loc (evar ~loc "fst") [ e ], eapply ~loc (evar ~loc "snd") [ e ]) in
    Some {e_of; e_to; e_acc=[]}
  | _ -> None

let camel_to_snake s =
  let n = String.length s in
  let b = Bytes.create (2*n) in
  let rec aux i j =
    if i = n then j
    else
    let c = String.get s i in
    let code = Char.code c in
    if code >= 65 && code <= 90 then (
      Bytes.set b j '_';
      Bytes.set b (j+1) (Char.chr (code + 32));
      aux (i+1) (j+2))
    else (
      Bytes.set b j c;
      aux (i+1) (j+1)) in
  let m = aux 0 0 in
  Bytes.(to_string @@ sub b 0 m)

let snake_to_camel s =
  let n = String.length s in
  let b = Bytes.create n in
  let rec aux i j =
    if i = n then j
    else
    let c = String.get s i in
    if c = '_' && i <> (n-1) then (
      let c = String.get s (i+1) in
      let code = Char.code c in
      begin if code >= 97 && code <= 122 then
          Bytes.set b j (Char.chr @@ code - 32)
        else Bytes.set b j c
      end;
      aux (i+2) (j+1))
    else (
      Bytes.set b j c;
      aux (i+1) (j+1)) in
  let m = aux 0 0 in
  Bytes.(to_string @@ sub b 0 m)

let core_attributes ~options l =
  List.fold_left (fun o a ->
    match a.attr_name.txt with
    | "callback" | "jsoo.callback" -> {o with c_cb = `cb}
    | "meth_callback" | "jsoo.meth_callback" -> {o with c_cb = `meth_cb}
    | "opt" | "jsoo.opt" -> {o with c_opt = `opt}
    | "aopt" | "jsoo.aopt" -> {o with c_opt = `aopt}
    | "optdef" | "jsoo.optdef" -> {o with c_opt = `optdef}
    | "ignore" | "jsoo.ignore" -> {o with c_ignore = true}
    | "array" | "jsoo.array" -> {o with c_array = true}
    | "assoc" | "jsoo.assoc" -> {o with c_assoc = true}
    | "number" | "jsoo.number" -> {o with c_number = true}
    | "enum" | "jsoo.enum" -> {o with c_variant = get_enum_attr a.attr_payload}
    | "case" | "jsoo.case" -> {o with c_variant = `case (get_string_attr a.attr_payload)}
    | "case_kind" | "jsoo.case_kind" -> {o with c_variant = `case_kind (get_string_attr a.attr_payload)}
    | "type" | "jsoo.type" ->
      let c_type = match get_ident_attr a.attr_payload with
        | None -> None
        | Some t -> Some (TT t) in
      {o with c_type}
    | "class_type" | "jsoo.class_type" ->
      let c_type = match get_ident_attr a.attr_payload with
        | None -> None
        | Some ct -> Some (CT ct) in
      {o with c_type}
    | "conv" | "jsoo.conv" -> {o with c_conv = get_conv_attr a.attr_payload}
    | "json" | "jsoo.json" -> {o with c_json = Some (get_expr_attr a.attr_payload)}
    | _ -> o) options l

let field_attributes ~options l =
  let f_key = match options.f_case with
    | `camel -> snake_to_camel options.f_key
    | `snake -> camel_to_snake options.f_key
    | `default -> options.f_key in
  let options = core_attributes ~options:{options with f_key} l in
  List.fold_left (fun o a ->
    match a.attr_name.txt with
    | "meth" | "jsoo.meth" -> {o with f_meth = true}
    | "mutable" | "jsoo.mutable" -> {o with f_prop = `mutable_}
    | "readonly" | "jsoo.readonly" -> {o with f_prop = `readonly}
    | "writeonly" | "jsoo.writeonly" -> {o with f_prop = `writeonly}
    | "key" | "jsoo.key" ->
      let f_key = match get_string_attr a.attr_payload with
        | None -> options.f_key | Some s -> s in
      {o with f_key}
    | "inherit" | "jsoo.inherit" -> {o with f_inherit = true}
    | "code" | "jsoo.code" -> {o with f_code = get_int_attr a.attr_payload}
    | "camel" | "jsoo.camel" -> {o with f_key = snake_to_camel o.f_key}
    | "snake" | "jsoo.snake" -> {o with f_key = camel_to_snake o.f_key}
    | "wrap" | "jsoo.wrap" -> { o with f_wrap = Some (get_string_attr a.attr_payload) }
    | _ -> o
  ) options l

let field_name ?(case=false) name =
  let name =
    if not case &&
       String.contains (String.sub name 0 (String.length name - 1))'_' then
      name ^ "_"
    else name in
  let code = Char.code @@ String.get name 0 in
  if code = 95 || (code >= 97 && code <= 122) then name
  else "_" ^ name

let get_new_name =
  let tmp_names = ref ([] : (string * int) list) in
  fun ?(suffix="_tmp") name ->
    let base = "_" ^ name ^ suffix in
    match List.assoc_opt name !tmp_names with
    | None ->
      tmp_names := (name, 0) :: !tmp_names; base
    | Some i ->
      tmp_names := (name, i+1) :: (List.remove_assoc name !tmp_names);
      base ^ string_of_int (i+1)

let get_tuple_name name = get_new_name ~suffix:"_tup" name
let get_variant_name name = get_new_name ~suffix:"_var" name
let get_object_name name = get_new_name ~suffix:"_obj" name
let get_record_name name = get_new_name ~suffix:"_rec" name

let str_of_expr e = Pprintast.string_of_expression e
let str_of_structure e = Pprintast.string_of_structure e

let str_of_pat e =
  Pprintast.pattern Format.str_formatter e;
  Format.flush_str_formatter ()

let str_of_ct e =
  Pprintast.class_type Format.str_formatter e;
  Format.flush_str_formatter ()

let remove_prefix s n = String.sub s n (String.length s - n)

let is_ok_field_name n s =
  if String.length s > n then
    let c = Char.code @@ String.get s n in
    (c >= 97 && c <= 122) || c = 95
  else false

let same_prefix l =
  let common_prefix s1 s2 =
    let n1 = String.length s1 in
    let n2 = String.length s2 in
    let rec aux i =
      if i < n1 && i < n2 && s1.[i] = s2.[i] then aux (i+1)
      else i, String.sub s1 0 i in
    aux 0 in
  let rec aux n pr = function
    | [] -> n, pr
    | h :: t ->
      let n, pr = common_prefix h pr in
      aux n pr t in
  let check n l = List.for_all (is_ok_field_name n) l in
  match l with
  | [] | [ _ ] -> 0
  | h :: t ->
    let n = fst (aux (String.length h) h t) in
    if check n l then n else 0

let prefix_length ~options l =
  match options.g_rm_prefix with
  | `bool false -> 0
  | `length l -> l
  | `prefix p -> String.length p
  | _ -> same_prefix l

let has_ezjs_min () =
  Sys.command "ocamlfind query -qe -qo -format %v ezjs_min" = 0

let is_variant_enum x =
  let l = match x with
    | `cons l -> List.map (fun pcd -> match pcd.pcd_args with
      | Pcstr_tuple l -> Some l
      | _ -> None) l
    | `variant l -> List.map (fun prf -> match prf.prf_desc with
      | Rtag (_, _, l) -> Some l
      | _ -> None) l in
  let rec aux has_fb l = match l with
    | Some [] :: q -> aux has_fb q
    | Some [ {ptyp_desc=Ptyp_constr ({txt=Lident "string"; _}, []); _} ] :: q when not has_fb -> aux true q
    | (None | Some _) :: _ -> false
    | [] -> true in
  aux false l
