let () =
  if Ppx_deriving_jsoo_lib.Common.has_ezjs_min () then
    Ppx_deriving_jsoo_lib.Ppx_deriver.main ()
  else
    let str_type_decl = Ppxlib.Deriving.Generator.make
        Ppx_deriving_jsoo_lib.Ppx_deriver.args_str
        (fun ~loc:_ ~path:_ _ _ _ _ _ _ _ _ _ _ _ _ -> []) in
    Ppxlib.Deriving.(ignore @@ add "jsoo" ~str_type_decl)
