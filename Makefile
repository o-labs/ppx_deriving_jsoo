all: build

build:
	@dune build src

dev:
	@dune build test --profile release

clean:
	@dune clean

install:
	@opam install .
