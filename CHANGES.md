## [0.3](https://gitlab.com/o-labs/ppx_deriving_jsoo/-/compare/0.2...0.3) (2022-12-14)

* Various improvements

## [0.2](https://gitlab.com/o-labs/ppx_deriving_jsoo/-/compare/0.1...0.2) (2020-12-18)

* Switch to ocaml-migrate-parsetree 2.0

## 0.1 (2020-12-15)

First Release
